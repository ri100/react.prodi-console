import React from 'react';
import {ProgressBox} from './Misc/ProgressBox.jsx';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {logData, errorData} from "../actions/jobLogActions";
import DivLink from './Misc/DivLink.jsx';
import "./ProgressLogConsole.css";

@connect((store) => {
    return {
        progressLog: store.progressLog,
        errorLog: store.errorLog,
    }
})
export default class ProgressLogConsole extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            consoleType: 1,
        }
    }

    componentDidMount = () => {
        this.props.dispatch(logData(this.props.jobId));
        this.props.dispatch(errorData(this.props.jobId));
    };

    displayConsole = () => {
        if (this.props.progressLog.progress) {
            return (<ProgressBox height="100%"/>);
        }
        const line = (row, index) => {
            if(typeof row.color != 'undefined') {
                return <div style={{color:row.color}} key={index}>{row.log}</div>
            } else {
                return <div key={index}>{row.log}</div>
            }
        }
        if(this.state.consoleType === 1) {
            return this.props.progressLog.data.map((row, index) => (
                line(row, index)
            ))
        } else {
            return this.props.errorLog.data.map((row, index) => (
                <div key={index}>
                    TYPE: {row.error.type}<br />
                    NODE: {row.error.node}<br />
                    MESSAGE: {row.error.message}<br />
                    DATA: {row.error.data}<br /><br />
                </div>
            ))
        }

    };

    displayTabs = () => {
        return <div className="tabs">
            <DivLink
                defaultSelected={this.state.consoleType === 1}
                color="black"
                notSelectedColor="#555"
                onClick={() => {
                    this.setState({consoleType: 1})
                }}>Console log</DivLink>

            <DivLink
                defaultSelected={this.state.consoleType === 2}
                color="black"
                notSelectedColor="#555"
                onClick={() => {
                    this.setState({consoleType: 2})
                }}>Error log</DivLink>
        </div>
    };

    render() {
        return (
            <div className="progressLogConsole">
                {this.displayTabs()}
                <div className="console">
                    {this.displayConsole()}
                </div>
            </div>
        );
    }
}
ProgressLogConsole.propTypes = {
    jobId: PropTypes.string.isRequired,
};

ProgressLogConsole.defaultProps = {};