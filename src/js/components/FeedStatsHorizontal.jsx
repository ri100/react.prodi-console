import React from 'react';
import PropTypes from 'prop-types';

export default class FeedStatsHorizontal extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (<div style={{backgroundColor: this.props.backgroundColor, color: this.props.color}}
                     className="downloadStats">
                <table style={{width:"100%", margin: 3}} className="stats">
                    <tbody>
                    <tr>
                        <td className="name">Date</td>
                        <td>{this.props.lastTime}</td>
                        <td className="name">Took time</td>
                        <td>{this.props.tookTime} sec.</td>
                        <td className="name">Inventory</td>
                        <td>{this.props.inventory}</td>
                        <td className="name">Status</td>
                        <td>{this.props.status}</td>
                    </tr>
                    <tr>
                        <td className="name">Deleted</td>
                        <td>{this.props.deletedItems}</td>
                        <td className="name">Errors</td>
                        <td>{this.props.errors}</td>
                        <td className="name">Matched</td>
                        <td> {this.props.matchedItems}</td>
                        <td className="name">Error message</td>
                        <td>{this.props.errorMessage}</td>
                    </tr>
                    <tr>
                        <td className="name">Inserted</td>
                        <td>{this.props.insertedItems}</td>
                        <td className="name">Updated</td>
                        <td>{this.props.updatedItems}</td>
                    </tr>
                    </tbody>
                </table>
        </div>  );
    };

}

FeedStatsHorizontal.propTypes = {
    lastTime: PropTypes.string.isRequired,
    tookTime: PropTypes.number.isRequired,
    inventory: PropTypes.number.isRequired,
    matchedItems: PropTypes.number.isRequired,
    updatedItems: PropTypes.number.isRequired,
    deletedItems: PropTypes.number.isRequired,
    insertedItems: PropTypes.number.isRequired,
    backgroundColor: PropTypes.string,
};
FeedStatsHorizontal.defaultProps = {
    errors: 0,
    color: 'rgb(63, 81, 181)'
};