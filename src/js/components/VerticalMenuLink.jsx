import React from 'react';
import { IconTooltip } from './Misc/IconTooltip.jsx';
import { Link } from 'react-router';
import "./VerticalMenuLink.css";

export default function VerticalMenuLink(props) {

    function renderShort() {
        return <IconTooltip label={props.label}>
            <Link to={props.link} onlyActiveOnIndex className="menuLink">
                {props.children}
            </Link>
        </IconTooltip>
    }

    function renderLong() {
        return <Link to={props.link} onlyActiveOnIndex className="menuLink">
            {props.children} <label className="menuLabel">{props.label}</label>
        </Link>
    }

    if(props.full) {
        return renderLong();
    }
    return renderShort();
};