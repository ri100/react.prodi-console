import React from 'react';
import store from "../store";
import { Provider } from "react-redux";
import Dialog from '@material-ui/core/Dialog';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import { hashHistory } from 'react-router';
import { searchProvider } from "../actions/providersActions";
import Hotkeys from 'react-hot-keys';
import TextField from "@material-ui/core/TextField";
import { lightBlue } from '@material-ui/core/colors';
import "./App.css";
import VerticalMenu from "./VerticalMenu.jsx";


const muiTheme = createMuiTheme({
    palette: {
        primary: {
            main: lightBlue[600],
        },
    },
});

export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            query: '',
            openSearch: false,
            fullMenu: false,
            menuWidth: 42
        };
    }

    onKeyDown(keyName, e, handle) {
        this.openSearchDialog();
    }

    openSearchDialog = () => {
        this.setState({
            openSearch: true
        });
    };

    search = (query) => {
        this.setState({
            openSearch: false
        });
        hashHistory.push('/providers');
        store.dispatch(searchProvider(query));
    };

    _handleTextFieldChange = (e) => {
        this.setState({
            query: e.target.value
        });
    };

    _menuToggle = () => {
        const newMenuState = !this.state.fullMenu;
        const newMenuWidth = (newMenuState) ? 180 : 42;
        this.setState({
            fullMenu: newMenuState,
            menuWidth: newMenuWidth
        });
    }

    render() {

        return (
            <Provider store={store}>
                <ThemeProvider theme={muiTheme}>
                    <Hotkeys keyName="escape"
                        onKeyDown={this.onKeyDown.bind(this)}>
                    <div id="app">

                        <VerticalMenu openSearchDialog={this.openSearchDialog}
                            width={this.state.menuWidth}
                            full={this.state.fullMenu} 
                            menuToggle={this._menuToggle}/>
                        
                        <div id="appContent" style={{paddingLeft: this.state.menuWidth+"px"}}>
                            <div className="appContent">{this.props.children}</div>
                        </div>

                        <Dialog aria-labelledby="simple-dialog-title" open={this.state.openSearch}>
                            <form style={{ margin: '20px', width: "500px" }} noValidate autoComplete="off">
                                <TextField
                                    label="Search"
                                    fullWidth={true}
                                    autoFocus={true}
                                    onKeyPress={(ev) => {
                                        if (ev.key === 'Enter') {
                                            this.search(this.state.query);
                                            ev.preventDefault();
                                        }
                                    }}
                                    onChange={this._handleTextFieldChange}
                                />
                            </form>
                        </Dialog>

                    </div>
                    </Hotkeys>
                </ThemeProvider>
            </Provider >
        );
    }
}