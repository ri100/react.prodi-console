import React from 'react'
import {ResponsiveContainer, PieChart, Pie, Sector, Cell} from 'recharts';

export const FailedVsOkPieChart = props => {

    const data = [{name: 'Group A', value: 400}, {name: 'Group B', value: 300},
        {name: 'Group C', value: 300}, {name: 'Group D', value: 200}];
    const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
    const RADIAN = Math.PI / 180;

    return (
        <ResponsiveContainer minHeight={240}>
            <PieChart
                width={800}
                height={400}
                style={{fontFamily: 'Lato, sans-serif', fontWeight: '400', fontSize: "12px"}}>
                <Pie
                    data={data}
                    cx={120}
                    cy={200}
                    innerRadius={60}
                    outerRadius={80}
                    fill="#8884d8"
                    paddingAngle={5}>
                    {data.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>) }
                </Pie>
            </PieChart>
        </ResponsiveContainer>)
    };