import { loadViaRedux } from '../logic/reduxAction.js';

export function searchOffers(query) {
    let url = 'http://localhost:5000/debug';
    let params = {
        query: query
    };
    return loadViaRedux('SEARCH', url, params);
}
