import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';

export default class DialogMsg extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            openDialog: this.props.open
        };
    }

    closeDialog = () => this.setState({openDialog: false});

    confirm = () => {
        this.closeDialog();
        this.props.onOK();
    };

    render() {
        return (
            <Dialog open={this.state.openDialog}>
                <DialogTitle id="form-dialog-title">{this.props.title}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {this.props.children}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.confirm} color="primary">
                        {this.props.OKLabel}
                    </Button>
                    <Button onClick={this.closeDialog} color="secondary">
                        {this.props.CancelLabel}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

DialogMsg.propTypes = {
    open: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    OKLabel: PropTypes.string,
    CancelLabel: PropTypes.string,
    onOK: PropTypes.func.isRequired
};

DialogMsg.defaultProps = {
    OKLabel: 'OK',
    CancelLabel: 'Cancel'
};
