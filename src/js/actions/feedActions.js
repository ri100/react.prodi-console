import { loadViaRedux } from '../logic/reduxAction.js';

export function feedDetails(id) {
    const url = 'api/Feed.getDetails';
    const params = {
        id: id
    };
    return loadViaRedux('SOURCE_DETAILS', url, params);
}

export function getFeedListForProvider(merchantId) {
    const url = 'api/Feed.getByMerchantHash';
    const params = {
        merchantHash: merchantId
    };
    return loadViaRedux('SOURCE_LIST', url, params);
}

export function getFeedCount(filter, progress=false) {
    const url = 'api/Feed.getNumberOfFilteredFeeds';
    const params = {
        filter: filter
    };
    return loadViaRedux('SOURCE_COUNT', url, params);
}

export function getRunningFeeds(filter, progress=false) {
    const url = 'api/Feed.getDownloading';
    const params = {
        random: Math.floor(Math.random() * 5000),
        limit: 60,
        filter: filter
    };

    return loadViaRedux('XML_LIST', url, params, progress);
}


export function getFailedFeeds(progress=false) {
    const url = 'api/Feed.getFailed';
    const params = {
        limit: 60
    };
    return loadViaRedux('FAILED_XML_LIST', url, params, progress);
}

export function getFinishedFeeds(progress=false) {
    const url = 'api/Feed.getFinished';
    const params = {
        limit: 60
    };
    return loadViaRedux('FINISHED_XML_LIST', url, params, progress);
}

export function getFeedsWithErrors(progress=false) {
    const url = 'api/Feed.getWithErrors';
    const params = {
        limit: 60
    };
    return loadViaRedux('FEEDS_WITH_ERRORS', url, params, progress);
}

export function getNotMatched() {
    const url = 'api/Feed.getNotMatched';
    const params = {
        limit: 14
    };
    return loadViaRedux('NOT_MATCHED_FEEDS', url, params);
}
