import React from 'react';
import {connect} from "react-redux";
import {searchOffers} from "../actions/searchActions";
import Cookies from 'universal-cookie';

const ValuePercent = (props) => (
    <div className="ValuePercentage">
        <span className="percentage">{props.percent}%</span> {props.label}
    </div>
);

const Row = (props) => (
    <div className="row">
        <div className="key">{props.label}:</div>
        <div className="value">
            {props.children}
        </div>
    </div>
);

const OfferItem = (props) => (
    <div className="offer">
        <img src={props.photo} style={{height: "120px", maxWidth: "150px", objectFit: "contain"}} alt="image"/>
        <h3 className="name">{props.name}</h3>
    </div>
);

const OfferItems = (props) => (
    <div>
        {
            props.data.map((type_data, type_index) => (
                <div>
                    <h2 key={type_index} className="header" style={{marginTop: "40px"}}>Inne produkty {type_data[0]}</h2>
                    <div>
                        {
                            type_data[1].map((row, index) => (
                                <OfferItem key={index} name={row.name} photo={row.photos[0]}/>
                            ))
                        }
                    </div>
                </div>
            ))
        }
    </div>
);

@connect((store) => {
    return {
        searchResult: store.search,
    }
})
export default class OfferSearch extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            query: ''
        };

        const cookies = new Cookies();
        const current = new Date();
        const nextYear = new Date();

        nextYear.setFullYear(current.getFullYear() + 10);

        console.log(cookies.get('myCat')); // Pacman
        cookies.set('myCat', 'Pacman', {path: '/', expires: nextYear});
        console.log(cookies.get('myCat')); // Pacman
    }

    search = (query) => {
        console.log(query);
        this.props.dispatch(searchOffers(query));

    };

    _handleTextFieldChange = (e) => {
        this.setState({
            query: e.target.value
        });
    };

    _handleKeyPress = (ev) => {
        if (ev.key === 'Enter') {
            this.search(this.state.query);
            ev.preventDefault();
        }
    };

    _handleSearchResult = () => {
        if (this.props.searchResult.progress === false) {
            return (
                <div style={{width: "100%"}}>
                    <section className="QueryAnalysis" style={{width: "250px"}}>
                        <h2 className="header">Query analysis</h2>
                        <div className="AnalysisTable">

                            <Row label="Context labels">
                                {
                                    this.props.searchResult.data['labels']['context'].map((row, index) => (
                                        <ValuePercent key={index}
                                                      label={row[1]}
                                                      percent={row[2]}/>

                                    ))
                                }
                            </Row>

                            <Row label="Product labels">
                                {
                                    this.props.searchResult.data['labels']['products'].map((row, index) => (
                                        <ValuePercent key={index}
                                                      label={row[1]}
                                                      percent={row[2]}/>

                                    ))
                                }
                            </Row>

                            <Row label="Used for">
                                {
                                    this.props.searchResult.data['labels']['action'].map((row, index) => (
                                        <ValuePercent key={index}
                                                      label={row[1]}
                                                      percent={row[2]}/>

                                    ))
                                }
                            </Row>

                            <Row label="Purpose">
                                {
                                    this.props.searchResult.data['labels']['rel'].map((row, index) => (
                                        <ValuePercent key={index}
                                                      label={row[1]}
                                                      percent={row[2]}/>

                                    ))
                                }
                            </Row>

                        </div>

                        <div className="AnalysisTable">
                            <Row label="Brands">
                                {
                                    this.props.searchResult.data['tags']['brands'].map((row, index) => (
                                        <ValuePercent key={index}
                                                      label={row[0]}
                                                      percent={row[1]}/>

                                    ))
                                }
                            </Row>
                            <Row label="Products">
                                {
                                    this.props.searchResult.data['tags']['products_1'].map((row, index) => (
                                        <ValuePercent key={index}
                                                      label={row[0]}
                                                      percent={row[1]}/>

                                    ))
                                }
                            </Row>
                            <Row label="Features">
                                {
                                    this.props.searchResult.data['tags']['features'].map((row, index) => (
                                        <ValuePercent key={index}
                                                      label={row[0]}
                                                      percent={row[1]}/>

                                    ))
                                }
                            </Row>
                            <Row label="Relations">
                                {
                                    this.props.searchResult.data['tags']['relations'].map((row, index) => (
                                        <ValuePercent key={index}
                                                      label={row[2] + " " + row[0]}
                                                      percent={row[1]}/>

                                    ))
                                }
                            </Row>
                        </div>
                    </section>
                    <section className="QueryAnalysis" style={{width: "250px"}}>
                        <h2 className="header">Query context</h2>
                        <div className="AnalysisTable">
                            <Row label="Context">
                                <ValuePercent label={this.props.searchResult.data['context'][1]}
                                              percent={this.props.searchResult.data['context'][2]}/>
                            </Row>
                            <Row label="Sex">
                                <ValuePercent label="Man"
                                              percent={this.props.searchResult.data['profile']['sex'][0]}/>
                                <ValuePercent label="Woman"
                                              percent={this.props.searchResult.data['profile']['sex'][1]}/>
                            </Row>
                            <Row label="Family">
                                <ValuePercent label="Yes"
                                              percent={this.props.searchResult.data['profile']['with_child'][0]}/>
                                <ValuePercent label="No"
                                              percent={this.props.searchResult.data['profile']['with_child'][1]}/>
                            </Row>
                            <Row label="Interests">
                                {
                                    this.props.searchResult.data['profile']['interests'].map((row, index) => (
                                        <ValuePercent key={index}
                                                      label={row[1]}
                                                      percent={row[2]}/>

                                    ))
                                }
                            </Row>
                        </div>
                    </section>
                    <section className="Search">
                        <h2 className="header">Most similar</h2>
                        <div>
                            {
                                this.props.searchResult.data['search'].map((row, index) => (
                                    <OfferItem key={index} name={row.name} photo={row.photos[0]}/>
                                ))
                            }
                        </div>
                        <h2 className="header" style={{marginTop: "40px"}}>Substitutes</h2>
                        <div>
                            {
                                this.props.searchResult.data['substitutes'].map((row, index) => (
                                    <OfferItem key={index} name={row.name} photo={row.photos[0]}/>
                                ))
                            }
                        </div>
                        <h2 className="header" style={{marginTop: "40px"}}>Complimentary</h2>
                        <div>
                            {
                                this.props.searchResult.data['complimentary'].map((row, index) => (
                                    <OfferItem key={index} name={row.name} photo={row.photos[0]}/>
                                ))
                            }
                        </div>

                        <OfferItems data={this.props.searchResult.data['same_labels']['action']}/>
                        <OfferItems data={this.props.searchResult.data['same_labels']['rel']}/>

                        <OfferItems data={this.props.searchResult.data['same_rels']['do']}/>

                    </section>
                    <div style={{"clear": "both"}}></div>
                </div>
            )
        }

    };

    render() {
        return (
            <div style={{height: '100%'}}>
                <div className="searchPane">
                    <div className="searchInputPane">
                        <form>
                            <input className="searchInput"
                                   type="text"
                                   onKeyPress={this._handleKeyPress}
                                   onChange={this._handleTextFieldChange}/>

                        </form>
                    </div>
                    <div className="searchResult">
                        {this._handleSearchResult()}
                    </div>
                </div>
            </div>
        );
    }
};