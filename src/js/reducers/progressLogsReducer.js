export default function(state={ data: [], error: null, progress: true }, action) {
    switch (action.type) {
        case "LOGS_LOADING":
            return {
                ...state,
                data: [],
                error: null,
                progress: true
            };
        case "LOGS_LOADED":
            return {
                ...state,
                data: action.data,
                error: null,
                progress: false
            };
        case "LOGS_FAILED":
            return {
                ...state,
                data: [],
                error: action.error,
                progress: true
            };
    }

    return state;
}