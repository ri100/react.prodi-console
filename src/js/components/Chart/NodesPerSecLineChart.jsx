import React from 'react';
import {ProgressBox} from '../Misc/ProgressBox.jsx'
import {
    ComposedChart,
    ResponsiveContainer,
    XAxis,
    Tooltip,
    Legend,
    Line
} from 'recharts';
import PropTypes from 'prop-types';

const NodesPerSecLineChart = props => {
    
    const renderNodesPerSecChart = (props) => {

        if (props.dataSource.progress) {
            const height = props.height+'px';
            return (<ProgressBox height={height}/>);
        }

        return (<ResponsiveContainer minHeight={props.height}>
            <ComposedChart
                style={{fontFamily: 'Lato, sans-serif', fontWeight: '400', fontSize: "12px"}}
                data={props.dataSource.data}>

                <XAxis dataKey="date"/>
                <Tooltip label="Legend"/>
                <Line
                    type="linear"
                    name="Nodes per seconds"
                    dataKey="nodesPerSec"
                    fill='#90A4AE'
                    dot={{r: 6, stroke: '#90A4AE', fill: "white"}} strokeWidth={3}
                />
                <Legend />
            </ComposedChart>
        </ResponsiveContainer>);
    };
    
    return renderNodesPerSecChart(props);
};

NodesPerSecLineChart.propTypes = {
    height: PropTypes.number,
    dataSource: PropTypes.object.isRequired
};
NodesPerSecLineChart.defaultProps = {
    height: '100%'
};

export default NodesPerSecLineChart;