import { loadViaRedux } from '../logic/reduxAction.js';

export function getDashboardChartData() {
    let url = 'api/FeedLog.aggregate';
    let params = {};
    return loadViaRedux('DASHBOARD_CHAR', url, params);
}

export function getDashboardCacheData() {
    let url = 'api/CacheStats.aggregate';
    let params = {};
    return loadViaRedux('DASHBOARD_CACHE', url, params);
}

export function getFeedsStats() {
    let url = 'api/Feed.stats';
    let params = {

    };
    return loadViaRedux('DASHBOARD_FEEDS_STATS', url, params, false);
}
