export default function(state={ data: [], error: null, progress: true }, action) {
    switch (action.type) {
        case "MERCHANT_JOBS_LOADING":
            return {
                ...state,
                data:[],
                error: null,
                progress: true
            };
        case "MERCHANT_JOBS_LOADED":
            return {
                ...state,
                data: action.data,
                error: null,
                progress: false
            };
        case "MERCHANT_JOBS_FAILED":
            return {
                ...state,
                data: [],
                error: action.error,
                progress: true
            };
    }

    return state;
}