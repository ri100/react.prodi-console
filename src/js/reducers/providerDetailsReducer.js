export default function(state={ data: {}, error: null, progress: true }, action) {
    switch (action.type) {
        case "PROVIDER_DETAILS_LOADING":
            return {
                ...state,
                data:{},
                error: null,
                progress: true
            };
        case "PROVIDER_DETAILS_LOADED":
            return {
                ...state,
                data: action.data,
                error: null,
                progress: false
            };
        case "PROVIDER_DETAILS_FAILED":
            return {
                ...state,
                data: {},
                error: action.error,
                progress: true
            };
    }

    return state;
}