import React from 'react';
import {ProgressBox} from './Misc/ProgressBox.jsx';
import PropTypes from 'prop-types';

export default class LogConsole extends React.Component {
    
    displayContent = () => {
        if(this.props.loading) {
            return (<ProgressBox height="100%"/>);
        }
        return this.props.logLines.map((row, index) => (
                row.log
            ))
    };
    
    render() {
        return(
            <pre className="console">
                { this.displayContent() }
            </pre>
        );
    }
}
LogConsole.propTypes = {
    logLines: PropTypes.array.isRequired,
};

LogConsole.defaultProps = {
    loading: true
};