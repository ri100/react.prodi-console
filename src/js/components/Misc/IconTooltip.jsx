import React from 'react';
import PropTypes from 'prop-types';

export const IconTooltip = props => {
    return (
        <div className="iconContainer">{props.children}<span className="float">{props.label}</span></div>
    );
};

IconTooltip.propTypes = {
    label: PropTypes.string.isRequired
};
