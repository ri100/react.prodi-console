export default function(state={ data: {}, error: null, progress: true }, action) {
    switch (action.type) {
        case "DOWNLOAD_LOG_LOADING":
            return {
                ...state,
                data:{},
                error: null,
                progress: true
            };
        case "DOWNLOAD_LOG_LOADED":
            return { 
                ...state,
                data: action.data,
                error: null,
                progress: false
            };
        case "DOWNLOAD_LOG_FAILED":
            return {
                ...state,
                data: {},
                error: action.error,
                progress: true
            };
    }

    return state;
}