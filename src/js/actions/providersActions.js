import { loadViaRedux } from '../logic/reduxAction.js';

export function  searchProvider(query) {
    let url = 'api/DataProvider.search';
    let params = {
        query: query
    };
    return loadViaRedux('PROVIDER_SEARCHING', url, params);
}

export function getProviderDetails(id) {
    let url = 'api/DataProvider.get';
    let params = {
        id: id
    };
    return loadViaRedux('PROVIDER_DETAILS', url, params);
}
