import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Button from '@material-ui/core/Button';
import {Tag} from '../Misc/Tag.jsx';
import PropTypes from 'prop-types';

export default class SimplifiedFeedLogTable extends React.Component {

    renderRows = () => {
        return this.props.data
            .map((row, index) => (
                    <TableRow key={index}>
                        <TableCell style={{
                            fontSize: "100%",
                            padding: "0 5px",
                            textAlign: 'center',
                            minWidth: "100px",
                            color: 'white'
                        }}>
                            <Tag title={row.download_status_message}
                                 backgroundColor={(row.download_status == 'failed') ? 'red' : (row.localCopy == true) ? '#f57c00' : '#388e3c'}>{(row.localCopy == true) ? 'file' : 'url'} / {row.download_status}
                            </Tag>
                        </TableCell>
                        <TableCell style={{fontSize: "100%", padding: "0 10px", textAlign: 'center'}}>
                            {row.name}
                        </TableCell>
                        <TableCell style={{fontSize: "100%", padding: "0 10px", textAlign: 'right'}}>
                            {row.matched}
                        </TableCell>
                        <TableCell style={{fontSize: "100%", padding: "0 10px", textAlign: 'right'}}>
                            {row.failed}
                        </TableCell>
                        <TableCell style={{fontSize: "100%", padding: "0 5px", width: "90px",}}>
                            <Button
                                variant="contained"
                                onClick={() => this.props.showDetailsEventFunc(row.id)}>Log</Button>
                        </TableCell>
                        <TableCell style={{fontSize: "100%", padding: "0 5px"}}>
                            <Button variant="contained"
                                    onClick={() => this.props.showErrorLogFunc(row.id)}
                                    style={{marginRight: '20px'}}>Error log</Button>
                        </TableCell>
                    </TableRow>
                )
            );
    };

    render() {
        return (
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell style={{
                            fontSize: "100%",
                            padding: "0 5px",
                            minWidth: "100px",
                            textAlign: 'center'
                        }}>Source</TableCell>
                        <TableCell
                            style={{fontSize: "100%", padding: "0 10px", textAlign: 'center'}}>Date</TableCell>
                        <TableCell style={{
                            fontSize: "100%",
                            padding: "0 10px",
                            textAlign: 'center'
                        }}>Matched</TableCell>
                        <TableCell style={{
                            fontSize: "100%",
                            padding: "0 10px",
                            textAlign: 'center'
                        }}>Failed</TableCell>
                        <TableCell style={{padding: "0 5px", width: "90px",}}/>
                        <TableCell style={{padding: "0 5px"}}/>

                    </TableRow>
                </TableHead>
                <TableBody>

                    {this.renderRows()}

                </TableBody>
            </Table>
        );
    }
}
SimplifiedFeedLogTable.propTypes = {
    data: PropTypes.array.isRequired,
    showDetailsEventFunc: PropTypes.func.isRequired,
    showErrorLogFunc: PropTypes.func.isRequired,
};