import React from 'react';
import ListOfFeedJobCards from './List/ListOfFeedJobCards.jsx';
import ProviderCard from './ProviderCard.jsx';
import {connect} from "react-redux";
import "./ProviderList.css";

@connect((store) => {
    return {
        currentProgress: false,
        providersList: store.providersList
    }
})
export default class ProviderList extends React.Component {

    displayMerchantHandler = () => {
        return this.props.providersList.data.map((row, index) => (
            <ProviderCard
                key={index}
                id={row.provider.hash}
                name={row.provider.name}
                url={row.provider.url}
                phone={row.provider.address.phone}
                email={row.provider.address.mail}
                street={row.provider.address.street}
                town={row.provider.address.town}
                active={row.provider.status.active}
            />
        ));
        
    };
    
    render() {
        return (
            <div className="ProviderList">

                <ListOfFeedJobCards
                    displayItemHandler={ this.displayMerchantHandler }
                    progress={this.props.providersList.progress}
                    height="100%"
                    overflowY="auto"
                />

            </div>
        );
    }
}