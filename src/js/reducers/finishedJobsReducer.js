export default function(state={ data: [], error: null, progress: true }, action) {
    switch (action.type) {
        case "FINISHED_XML_LIST_LOADING":
            return {
                ...state,
                data:[],
                error: null,
                progress: true
            };
        case "FINISHED_XML_LIST_LOADED":
            return {
                ...state,
                data: action.data,
                error: null,
                progress: false
            };
        case "FINISHED_XML_LIST_FAILED":
            return {
                ...state,
                data: [],
                error: action.error,
                progress: true
            };
    }

    return state;
}