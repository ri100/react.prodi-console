import React from 'react';
import Paper from '@material-ui/core/Paper';
import FeedTopInfo from './FeedTopInfo.jsx';
import {ProgressBox} from './Misc/ProgressBox.jsx';
import DivLink from './Misc/DivLink.jsx';
import FeedLogTable from './Table/FeedLogTable.jsx';
import {getJobsChartData} from "../actions/feedLogActions";
import {feedDetails} from "../actions/feedActions";
import {connect} from "react-redux";
import {hashHistory} from "react-router";
import FeedInfo from './Dialog/FeedInfo.jsx';

import {
    ComposedChart,
    Bar,
    ResponsiveContainer,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
} from 'recharts';

@connect((store) => {
    return {
        currentProgress: false,
        feedDetails: store.feedDetails,
        progressLog: store.progressLog,
        errorLog: store.errorLog,
        jobChartData: store.jobChartData
    }
})
export default class FeedDetails extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            jobId: null,
            chartType: 1
        };
    }

    componentDidMount = () => {
        this.props.dispatch(feedDetails(this.props.params.feedId));
        this.props.dispatch(getJobsChartData(this.props.params.feedId));
    };

    handleClick = (e) => {
        this.displayJobDetails(e.id);
    };

    displayJobDetails = (jobId) => {
        hashHistory.push('/jobConsole/' + this.props.params.feedId + '/' + jobId);
    };

    renderTimeChart = () => {
        if (this.props.jobChartData.progress) {
            return (
                <ProgressBox height="240px"/>
            );
        }
        return (<ResponsiveContainer minHeight={240}>
            <ComposedChart
                style={{
                    backgroundColor: '#ECEFF1',
                    fontFamily: 'Lato, sans-serif',
                    fontWeight: '400',
                    fontSize: "12px"
                }}
                data={this.props.jobChartData.data} margin={{top: 40, right: 20, left: 0, bottom: 20}}>
                <XAxis dataKey="name"/>
                <YAxis/>
                <CartesianGrid strokeDasharray="3 3"/>
                <Tooltip label="Legend"/>
                <Legend/>

                <Line type="linear" name="Elapsed time" dataKey="elapsedTime"
                      stroke="#757575" dot={{r: 6}} strokeWidth={2} onClick={this.handleClick}/>

            </ComposedChart>
        </ResponsiveContainer>);
    };

    renderSpeedChart = () => {
        if (this.props.jobChartData.progress) {
            return (
                <ProgressBox height="240px"/>
            );
        }
        return (<ResponsiveContainer minHeight={240}>
            <ComposedChart
                style={{
                    backgroundColor: '#ECEFF1',
                    fontFamily: 'Lato, sans-serif',
                    fontWeight: '400',
                    fontSize: "12px"
                }}
                data={this.props.jobChartData.data} margin={{top: 40, right: 20, left: 0, bottom: 20}}>
                <XAxis dataKey="name"/>
                <YAxis/>
                <CartesianGrid strokeDasharray="3 3"/>
                <Tooltip label="Legend"/>
                <Legend/>

                <Line type="linear" name="Nodes per sec" dataKey="nodesPerSec"
                      stroke="#757575" dot={{r: 6}} strokeWidth={2} onClick={this.handleClick}/>

            </ComposedChart>
        </ResponsiveContainer>);
    };

    renderDataChart = () => {
        if (this.props.jobChartData.progress) {
            return (
                <ProgressBox height="240px"/>
            );
        }
        return (<ResponsiveContainer minHeight={240}>
            <ComposedChart
                style={{
                    backgroundColor: '#ECEFF1',
                    fontFamily: 'Lato, sans-serif',
                    fontWeight: '400',
                    fontSize: "12px"
                }}
                data={this.props.jobChartData.data} margin={{top: 40, right: 20, left: 0, bottom: 20}}>
                <XAxis dataKey="name"/>
                <YAxis/>
                <CartesianGrid strokeDasharray="3 3"/>
                <Tooltip label="Legend"/>
                <Legend/>

                <Line type='monotone' name="Matched items" dataKey='matched' stroke="#424242"
                      dot={{r: 6}} strokeWidth={2} onClick={this.handleClick}/>

            </ComposedChart>
        </ResponsiveContainer>);
    };

    renderCacheChart = () => {
        if (this.props.jobChartData.progress) {
            return (
                <ProgressBox height="240px"/>
            );
        }
        return (<ResponsiveContainer minHeight={240}>
            <ComposedChart
                style={{
                    backgroundColor: '#ECEFF1',
                    fontFamily: 'Lato, sans-serif',
                    fontWeight: '400',
                    fontSize: "12px"
                }}
                data={this.props.jobChartData.data} margin={{top: 40, right: 20, left: 0, bottom: 20}}>
                <XAxis dataKey="name"/>
                <YAxis/>
                <CartesianGrid strokeDasharray="3 3"/>
                <Tooltip label="Legend"/>
                <Legend/>

                <Line name="Not synchronized data between database and cache" dataKey='unSyncDatabaseData' stroke="red"
                      dot={{r: 6}} strokeWidth={2} onClick={this.handleClick}/>

                <Line name="Not synchronized data inside cache" dataKey='unSyncCacheData' stroke="#424242"
                      dot={{r: 6}} strokeWidth={2} onClick={this.handleClick}/>

                <Line name="Restored data from database" dataKey='restoredCacheFromDatabase' stroke="#AD1457"
                      dot={{r: 6}} strokeWidth={2} onClick={this.handleClick}/>

                <Line name="Duplicated ids in database" dataKey='duplicatedIdsInDatabase' stroke="#D81B60"
                      dot={{r: 6}} strokeWidth={2} onClick={this.handleClick}/>

            </ComposedChart>
        </ResponsiveContainer>);
    };

    renderDeltaChart = () => {
        if (this.props.jobChartData.progress) {
            return (
                <ProgressBox height="240px"/>
            );
        }
        return (<ResponsiveContainer minHeight={240}>
            <ComposedChart
                style={{
                    backgroundColor: '#ECEFF1',
                    fontFamily: 'Lato, sans-serif',
                    fontWeight: '400',
                    fontSize: "12px"
                }}
                data={this.props.jobChartData.data} margin={{top: 40, right: 20, left: 0, bottom: 20}}>
                <XAxis dataKey="name"/>
                <YAxis/>
                <CartesianGrid strokeDasharray="3 3"/>
                <Tooltip label="Legend"/>
                <Legend/>

                <Bar stackId="all" name="Deleted items" dataKey="deleted"
                     fill="#F8BBD0" onClick={this.handleClick}/>
                <Bar stackId="all" name="Updated items" dataKey="updated"
                     fill="#C5E1A5" onClick={this.handleClick}/>
                <Bar stackId="all" name="Inserted items" dataKey="inserted"
                     fill="#81C784" onClick={this.handleClick}/>

                <Line strokeWidth={2} type="monotone" dot={{r: 6}} name="Failed items" dataKey="failed"
                      stroke="#4e342e" onClick={this.handleClick}/>

                <Line strokeWidth={2} type="monotone" dot={{r: 6}} name="Restored items from database"
                      dataKey="restoredCacheFromDatabase"
                      stroke="#BF360C" onClick={this.handleClick}/>

            </ComposedChart>
        </ResponsiveContainer>);
    };

    renderTable = () => {
        if (this.props.jobChartData.progress) {
            return (
                <ProgressBox height="50px"/>
            );
        }
        return <FeedLogTable
            data={this.props.jobChartData.data}
        />
    };

    renderChart = () => {
        if (this.state.chartType === 1) {
            return this.renderDeltaChart();
        } else if (this.state.chartType === 2) {
            return this.renderSpeedChart();
        } else if (this.state.chartType === 3) {
            return this.renderTimeChart();
        } else if (this.state.chartType === 4) {
            return this.renderDataChart();
        } else if (this.state.chartType === 5) {
            return this.renderCacheChart();
        }
    };

    renderTopInfo = () => {
        if (this.props.feedDetails.progress === false) {
            return <FeedTopInfo
                status={this.props.feedDetails.data.process.status.code}
                feedId={this.props.params.feedId}
                providerId={this.props.feedDetails.data.merchant.hash}
                name={this.props.feedDetails.data.merchant.name}
                url={this.props.feedDetails.data.download.url}
                schema={this.props.feedDetails.data.download.schema}
                hash={this.props.feedDetails.data.merchant.hash}
                records={this.props.feedDetails.data.parse.matched / 1000}
                date={this.props.feedDetails.data.download.date}
                active={this.props.feedDetails.data.process.active}
                download_status={this.props.feedDetails.data.download.status.code}
                downlaod_status_message={this.props.feedDetails.data.download.status.message}
                onFeedFinished={() => {
                    this.props.dispatch(getJobsChartData(this.props.params.feedId));
                }}
            >
                <DivLink
                    defaultSelected={this.state.chartType === 1}
                    onClick={() => {
                        this.setState({chartType: 1});
                    }}>Delta</DivLink>

                <DivLink
                    defaultSelected={this.state.chartType === 4}
                    onClick={() => {
                        this.setState({chartType: 4});
                    }}>Data</DivLink>

                <DivLink
                    defaultSelected={this.state.chartType === 5}
                    onClick={() => {
                        this.setState({chartType: 5});
                    }}>Cache</DivLink>

                <DivLink
                    defaultSelected={this.state.chartType === 2}
                    onClick={() => {
                        this.setState({chartType: 2});
                    }}>Speed</DivLink>

                <DivLink
                    defaultSelected={this.state.chartType === 3}
                    onClick={() => {
                        this.setState({chartType: 3});
                    }}>Time</DivLink>

                <DivLink
                    defaultSelected={this.state.chartType === 6}
                    onClick={() => {
                        this.setState({chartType: 6});
                    }}>Info</DivLink>

            </FeedTopInfo>
        }
    };

    renderBottom = () => {
        if(this.state.chartType !== 6) {
            return <div style={{height:"100%"}}>
                <div className="downloadDetails">
                    {this.renderChart()}
                </div>
                <div style={{
                    fontSize: "80%",
                    padding: "0 20px",
                    height: "calc(100% - 390px)",
                    overflow: 'auto',
                    backgroundColor: 'white',
                    boxSizing: 'border-box'
                }}>
                    {this.renderTable()}
                </div>
            </div>
        }
        if(this.props.feedDetails.progress === false) {
            return <div style={{backgroundColor: 'white', height: "100%"}}>
                <FeedInfo id={this.props.params.feedId} feedData={this.props.feedDetails}/>
            </div>
        }
    }

    render() {
        return (
            <div style={{height: '100%', overflow: 'hidden'}}>

                <Paper elevation={2} style={{
                    width: '100%',
                    backgroundColor: '#1565C0',
                    color: 'white',
                    position: 'relative',
                    zIndex: 10,
                    height: '150px',
                }}>
                    {this.renderTopInfo()}
                </Paper>

                {this.renderBottom()}

            </div>
        );
    };
}

FeedDetails.defaultProps = {
    backgroundColor: 'rgb(63, 81, 181)',
    color: 'white',
    id: null,
    name: 'n/a',
    url: 'n/a',
    street: 'n/a',
    town: 'n/a',
    email: 'n/a',
    phone: 'n/a',
    image: 'n/a',
};
