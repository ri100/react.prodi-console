import React from 'react';

export default class Header extends React.Component {
    render() {
        return(
            <h1 style={this.props.style} className="header">{this.props.children}</h1>
        );
    }
}