export default function(state={ data: [], error: null, progress: true }, action) {
    switch (action.type) {
        case "JOB_CHART_DATA_LOADING":
            return {
                ...state,
                data:[],
                error: null,
                progress: true
            };
        case "JOB_CHART_DATA_LOADED":
            return {
                ...state,
                data: action.data,
                error: null,
                progress: false
            };
        case "JOB_CHART_DATA_FAILED":
            return {
                ...state,
                data: [],
                error: action.error,
                progress: false
            };
    }

    return state;
}