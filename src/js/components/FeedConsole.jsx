import React from 'react';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import FeedTopInfo from './FeedTopInfo.jsx';
// import LogConsole from './LogConsole.jsx';
import ProgressLogConsole from './ProgressLogConsole.jsx';
import ErrorConsole from './ErrorConsole.jsx';
import {ProgressBox} from './Misc/ProgressBox.jsx';
import SimplifiedFeedLogTable from './Table/SimplifiedFeedLogTable.jsx';
import {getJobsChartData} from "../actions/feedLogActions";
import {logData, errorData} from "../actions/jobLogActions";
import {feedDetails} from "../actions/feedActions";
import {connect} from "react-redux";

@connect((store) => {
    return {
        // currentProgress: false,
        feedDetails: store.feedDetails,
        // feedLog: store.feedLog,
        // progressLog: store.progressLog,
        errorLog: store.errorLog,
        jobChartData: store.jobChartData
    }
})
export default class FeedConsole extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            consoleType: 1,
            jobId: this.props.params.jobId
        };
    }

    componentWillMount = () => {
        this.props.dispatch(feedDetails(this.props.params.feedId));
        this.props.dispatch(getJobsChartData(this.props.params.feedId));
        // this.props.dispatch(logData(this.props.params.jobId));
    };

    displayErrorDetails = (jobId) => {
        this.props.dispatch(errorData(jobId));
        this.setState({
            consoleType: 2
        });
    };

    displayLogDetails = (jobId) => {
        console.log(jobId);
        // this.props.dispatch(logData(jobId));
        this.setState({
            jobId: jobId,
            consoleType: null
        });
        this.setState({
            jobId: jobId,
            consoleType: 1
        });
    };
    
    renderTable = () => {
        if (this.props.jobChartData.progress) {
            return (
                <ProgressBox height="100%"/>
            );
        }
        return <SimplifiedFeedLogTable 
            data={this.props.jobChartData.data} 
            showDetailsEventFunc={this.displayLogDetails}
            showErrorLogFunc={this.displayErrorDetails}
        />
    };

    renderLog = (jobId) => {
        if(this.state.consoleType === 1) {
            return <ProgressLogConsole jobId={jobId}/>
        }
        if (Array.isArray(this.props.errorLog.data)) {
            return (<ErrorConsole logLines={this.props.errorLog.data}
                                  loading={this.props.errorLog.progress}/>
            );
        }
    };
    
    renderTopInfo = () => {
        if (this.props.feedDetails.progress === false) {
            return <FeedTopInfo
                feedId={this.props.params.feedId}
                providerId={this.props.feedDetails.data.merchant.hash}
                name={this.props.feedDetails.data.merchant.name}
                url={this.props.feedDetails.data.download.url}
                schema={this.props.feedDetails.data.download.schema}
                hash={this.props.feedDetails.data.merchant.hash}
                records={this.props.feedDetails.data.parse.matched / 1000}
                date={this.props.feedDetails.data.download.date}
                active={this.props.feedDetails.data.process.active}
                download_status={this.props.feedDetails.data.download.status.code}
                downlaod_status_message={this.props.feedDetails.data.download.status.message}
            >
                
            </FeedTopInfo>
        }
    };

    render() {
        return (
            <div style={{height: '100%', overflow:'hidden'}}>

                <Paper elevation={2} style={{
                    width: '100%',
                    backgroundColor: '#1565C0',
                    color: 'white',
                    position: 'relative',
                    zIndex: 10,
                    height: '150px'
                }}>
                    {this.renderTopInfo()}
                </Paper>
                <div style={{height: 'calc(100% - 150px)'}}>
                    <div style={{display:'block', width: '100%', height: '100%'  }}>
                        <div style={{float:'left', width: '50%', verticalAlign: 'top', height: "100%", overflow: 'auto', backgroundColor: 'white'}}>
                            { this.renderTable() }
                        </div>
                        <div style={{float:'left', width: '50%', verticalAlign: 'top', height: "100%", backgroundColor: 'black'}}>
                            { this.renderLog(this.state.jobId) }
                        </div>
                    </div>
                </div>
            </div>
        );
    };
}

FeedConsole.defaultProps = {
    backgroundColor: 'rgb(63, 81, 181)',
    color: 'white',
    id: null,
    name: 'n/a',
    url: 'n/a',
    street: 'n/a',
    town: 'n/a',
    email: 'n/a',
    phone: 'n/a',
    image: 'n/a',
};
