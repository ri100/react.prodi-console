export default function(state={ data: [], error: null, progress: true }, action) {
    switch (action.type) {
        case "DASHBOARD_CACHE_LOADING":
            return {
                ...state,
                data: [],
                error: null,
                progress: true
            };
        case "DASHBOARD_CACHE_LOADED":
            return {
                ...state,
                data: action.data,
                error: null,
                progress: false
            };
        case "DASHBOARD_CACHE_FAILED":
            return {
                ...state,
                data: [],
                error: action.error,
                progress: true
            };
    }

    return state;
}