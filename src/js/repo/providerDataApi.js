import { loadJson } from '../logic/axiosLoad';

export function runAllFeedsFromProvider(id, onSuccess, onError) {
    var url = 'api/DataProvider.startJob';
    var params = {
        id: id
    };
    
    loadJson(url, params, onSuccess, onError);
}