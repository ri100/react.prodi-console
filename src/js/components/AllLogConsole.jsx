import React from 'react';
import {ProgressBox} from './Misc/ProgressBox.jsx';
import "./AllLogConsole.css";
import {connect} from "react-redux";
import {logsData} from "../actions/jobLogActions";
import ProgressLogConsole from "./ProgressLogConsole.jsx";
import DialogMsgSingleButton from "./Dialog/DialogMsgSingleButton.jsx";

@connect((store) => {
    return {
        logs: store.progressLogs
    }
})
export default class AllLogConsole extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            jobId: "0"
        };
    }

    componentDidMount = () => {
        this.props.dispatch(logsData());
        this.interval = setInterval(() => {
            this.props.dispatch(logsData());
        }, 5000);
    };

    componentWillUnmount = () => {
        clearInterval(this.interval);
    };

    openConsole = (jobId) => {
        return (e) => {
            this.setState({jobId:jobId});
            e.preventDefault();
            e.stopPropagation();
            this.dialog.setState({
                openDialog: true
            });
        }
    }

    displayContent = () => {
        if(this.props.logs.progress) {
            return (<ProgressBox height="100%"/>);
        }

        const line = (row, index) => {
            let rowLine = <span onClick={this.openConsole(row.job.id)} style={{color: "#"+row.job.id.substring(0,6)}}>{row.job.id}</span>
            if(typeof row.color != 'undefined') {
                return <div style={{color:row.color}} key={index}>{rowLine} | {row.log}</div>
            } else {
                return <div key={index}>{rowLine} | {row.log}</div>
            }
        }
        return this.props.logs.data.map((row, index) => (
            line(row,index)
        ))
    };
    
    render() {
        return(
            <div className="AllLogConsole">
                <DialogMsgSingleButton
                    title="Console"
                    open={false}
                    OKLabel="Close"
                    ref={(dialog) => {
                        this.dialog = dialog;
                    }}>
                    <ProgressLogConsole jobId={this.state.jobId} />
                </DialogMsgSingleButton>
                <div className="console">{ this.displayContent() }</div>
            </div>
        );
    }
}
AllLogConsole.propTypes = {

};

AllLogConsole.defaultProps = {
    loading: true
};