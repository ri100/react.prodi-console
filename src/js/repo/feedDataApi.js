import { loadJson } from '../logic/axiosLoad';

export function toggleActiveFeed(feedId, active, onSuccess, onError) {
    var url = 'api/Feed.toggleActive';
    var params = {
        feedId: feedId,
        active: active
    };

    loadJson(url, params, onSuccess, onError);
}

export function getFeedAggregate(onSuccess, onError) {
    var url = 'api/FeedLog.aggregate';
    var params = {};
    loadJson(url, params, onSuccess, onError);
}

export function runFeed(id, onSuccess, onError) {
    var url = 'api/Feed.startJob';
    var params = {
        id: id
    };

    loadJson(url, params, onSuccess, onError);
}

export function getDetails(id, onSuccess, onError) {
    var url = 'api/Feed.getDetails';
    var params = {
        id: id
    };

    loadJson(url, params, onSuccess, onError);
}