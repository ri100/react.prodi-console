import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import {connect} from "react-redux";
import {getDashboardChartData, getDashboardCacheData, getFeedsStats} from "../actions/dashboardActions";
import Header from './Misc/Header.jsx'
import DashboardCounters from './DashboardCounters.jsx';
import TimeLineChart from './Chart/TimeLineChart.jsx';
import JobLineChart from './Chart/JobLineChart.jsx';
import NodesPerSecLineChart from './Chart/NodesPerSecLineChart.jsx';
import MatchedBarChart from './Chart/MatchedBarChart.jsx';
import InsertedBarChart from './Chart/InsertedBarChart.jsx'
import CacheBarChart from './Chart/CacheBarChart.jsx';
import CacheFailureBarChart from './Chart/CacheFailureBarChart.jsx';
import SizeLineChart from './Chart/SizeLineChart.jsx';
import AvgTimeLineChart from './Chart/AvgTimeLineChart.jsx';

@connect((store) => {
    return {
        dashboardChart: store.dashboardChart,
        dashboardCache: store.dashboardCache,
    }
})
export default  class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            openSnackBar: false,
            snackMessage: ''
        }
    }

    componentDidMount = () => {
        this.props.dispatch(getDashboardChartData());
        this.props.dispatch(getDashboardCacheData());
    };

    render() {

        const chartContainer = {
            width: "100%",
            border: 0,
            display: "block",
            boxSizing: "border-box"
        };

        return (
            <div className="dataPane">
                <DashboardCounters
                    onSuccess={(data) => {
                        this.setState(
                            {
                                openSnackBar: true,
                                snackMessage: 'Accepted ' + data + ' jobs'
                            });
                    }}
                    onError={(error) => {
                        this.setState(
                            {
                                openSnackBar: true,
                                snackMessage: error.message
                            });
                    }}
                />

                <div style={{height: "calc(100% - 140px)", overflow: 'auto',  boxSizing: 'border-box'}}>
                    <div style={{...chartContainer, backgroundColor: '#e5e5e5'}}>
                        <div style={{width: "75%", display: "block", float: "left"}}>
                            <Header><span style={{fontWeight: '400', color: 'black'}}>Jobs</span> executed</Header>
                            <JobLineChart dataSource={this.props.dashboardChart} height={240}/>
                        </div>
                        <div style={{width: "24%", display: "block", float: "right"}}>
                            <Header><span style={{fontWeight: '400'}}>Retrieved</span> records</Header>
                            <MatchedBarChart dataSource={this.props.dashboardChart} height={240}/>
                        </div>
                        <div style={{clear: "both"}}></div>
                    </div>
                    <div style={chartContainer}>
                        <div style={{width: "50%", display: "block", float: "left"}}>
                            <Header><span style={{fontWeight: '400'}}>Changed</span> records</Header>
                            <InsertedBarChart dataSource={this.props.dashboardChart} height={240}/>
                        </div>
                        <div style={{width: " 25%", display: "block", float: "left"}}>
                            <Header><span style={{fontWeight: '400'}}>Cache</span> resync records</Header>
                            <CacheBarChart dataSource={this.props.dashboardCache} height={240}/>
                        </div>
                        <div style={{width: " 25%", display: "block", float: "left"}}>
                            <Header><span style={{fontWeight: '400'}}>Cache</span> incidents</Header>
                            <CacheFailureBarChart dataSource={this.props.dashboardCache} height={240}/>
                        </div>
                        <div style={{clear: "both"}}></div>
                    </div>

                    <div style={chartContainer}>
                        <div style={{width: " 25%", display: "block", float: "left"}}>
                            <Header><span style={{fontWeight: '400'}}>Data</span> size <span style={{fontSize: '60%'}}>in mega bytes</span></Header>
                            <SizeLineChart dataSource={this.props.dashboardChart} height={150}/>
                        </div>
                        <div style={{width: " 25%", display: "block", float: "left"}}>
                            <Header><span style={{fontWeight: '400'}}>Time</span> consumed <span
                                style={{fontSize: '60%'}}>in min.</span></Header>
                            <TimeLineChart dataSource={this.props.dashboardChart} height={150}/>
                        </div>
                        <div style={{width: " 25%", display: "block", float: "left"}}>
                            <Header><span style={{fontWeight: '400'}}>Time</span> per job <span
                                style={{fontSize: '60%'}}>in sec.</span></Header>
                            <AvgTimeLineChart dataSource={this.props.dashboardChart} height={150}/>
                        </div>
                        <div style={{width: " 25%", display: "block", float: "left"}}>
                            <Header><span style={{fontWeight: '400'}}>Speed</span> in nodes<span
                                style={{fontSize: '60%'}}> per sec.</span></Header>
                            <NodesPerSecLineChart dataSource={this.props.dashboardChart} height={150}/>
                        </div>
                        <div style={{clear: "both"}}></div>
                    </div>
                </div>

                <Snackbar
                    open={this.state.openSnackBar}
                    message={this.state.snackMessage}
                    autoHideDuration={4000}
                />
            </div>
        );
    };
}