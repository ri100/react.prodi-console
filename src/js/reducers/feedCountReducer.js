export default function(state={ data: [], error: null, progress: true }, action) {
    switch (action.type) {
        case "SOURCE_COUNT_LOADING":
            return {
                ...state,
                data:[],
                error: null,
                progress: true
            };
        case "SOURCE_COUNT_LOADED":
            return {
                ...state,
                data: action.data,
                error: null,
                progress: false
            };
        case "SOURCE_COUNT_FAILED":
            return {
                ...state,
                data: [],
                error: action.error,
                progress: true
            };
    }

    return state;
}