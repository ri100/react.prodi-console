import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import Switch from '@material-ui/core/Switch';
import Snackbar from '@material-ui/core/Snackbar';
import {Link} from 'react-router';
import {toggleActiveFeed} from '../../repo/feedDataApi.js';
import PropTypes from 'prop-types';


export default class FeedTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            openErrorSnackbar: false,
            errorSnackbarMessage: 'n/a'
        }
    }

    displayError = (error) => {
        this.setState({
            openErrorSnackbar: true,
            errorSnackbarMessage: error.message
        });
    };

    activeToggle = (toggled, feedId) => {
        toggleActiveFeed(
            feedId,
            toggled,
            () => {
                console.log("Feed " + feedId + "toggled")
            },
            this.displayError);
    };

    renderButton = (row) => {
        if (typeof row.process.job != 'undefined') {
            return <Link to={"/feedDetails/" + row.id + "/" + row.process.job.lastId}>
                <Button
                    variant="contained"
                    onClick={() => this.props.showDetailsEventFunc(row.id)}>Details</Button>
            </Link>
        }
    };

    renderRows = () => {
        if (typeof this.props.data != 'undefined') {
            return this.props.data
                .map((row, index) => (
                        <TableRow key={index}>
                            <TableCell
                                style={{padding: "0px 3px", width: "50px", fontSize: "100%"}}><Switch
                                defaultChecked={row.process.active} onChange={(e, toggled) => {
                                this.activeToggle(toggled, row.id)
                            }}/></TableCell>

                            <TableCell
                                style={{
                                    padding: "0px 3px",
                                    width: "140px",
                                    fontSize: "100%"
                                }}>{row.merchant.name}</TableCell>
                            <TableCell
                                style={{padding: "0px 3px", fontSize: "100%"}}>{row.download.url}</TableCell>
                            <TableCell
                                style={{padding: "0px 3px", fontSize: "100%"}}>{row.download.schema}</TableCell>
                            <TableCell
                                style={{
                                    padding: "0px 3px",
                                    width: "60px",
                                    fontSize: "100%",
                                    textAlign: "right"
                                }}>{row.parse.matched}</TableCell>
                            <TableCell
                                style={{
                                    padding: "0px 3px",
                                    width: "60px",
                                    fontSize: "100%",
                                    textAlign: "right"
                                }}>{row.download.time}</TableCell>
                            <TableCell
                                style={{
                                    padding: "0px 3px",
                                    width: "100px",
                                    fontSize: "100%",
                                    textAlign: "right"
                                }}>{row.download.size}</TableCell>
                            <TableCell
                                style={{
                                    padding: "0px 3px",
                                    width: "60px",
                                    fontSize: "100%",
                                    textTransform: 'uppercase'
                                }}>{row.download.status.code}</TableCell>
                            <TableCell style={{
                                padding: "0px 3px",
                                width: "80px",
                                fontSize: "100%",
                                textTransform: 'uppercase'
                            }}>{row.parse.status.code}</TableCell>
                            <TableCell component="th" style={{width: "100px", fontSize: "100%"}}>
                                {this.renderButton(row)}
                            </TableCell>
                        </TableRow>
                    )
                );
        }
    };

    render() {
        return (
            <div>
                <Table style={this.props.style}>
                    <TableHead>
                        <TableRow>
                            <TableCell style={{padding: "0px 3px", width: "50px", fontSize: "100%"}}
                                       tooltip="Is feed active">On/Off</TableCell>
                            <TableCell style={{padding: "0px 3px", width: "140px", fontSize: "100%"}}>Provider
                                name</TableCell>
                            <TableCell style={{padding: "0px 3px", fontSize: "100%"}} tooltip="Source URL">Feed
                                URL</TableCell>
                            <TableCell style={{padding: "0px 3px", fontSize: "100%"}}
                                       tooltip="The way the data is parsed">Schema</TableCell>
                            <TableCell
                                style={{padding: "0px 3px", width: "60px", fontSize: "100%", textAlign: "center"}}
                                tooltip="Matched items in XML">Matched</TableCell>
                            <TableCell
                                style={{padding: "0px 3px", width: "60px", fontSize: "100%", textAlign: "center"}}
                                tooltip="Time consumed during processing">Time</TableCell>
                            <TableCell
                                style={{padding: "0px 3px", width: "100px", fontSize: "100%", textAlign: "center"}}
                                tooltip="Downloaded file size">File size</TableCell>
                            <TableCell
                                style={{padding: "0px 3px", width: "60px", fontSize: "100%", textAlign: "center"}}
                                tooltip="Download status">Status</TableCell>
                            <TableCell
                                style={{padding: "0px 3px", width: "80px", fontSize: "100%", textAlign: "center"}}
                                tooltip="Data parse status">Parse status</TableCell>
                            <TableCell style={{width: "100px"}}/>
                        </TableRow>
                    </TableHead>
                    <TableBody>

                        {this.renderRows()}

                    </TableBody>
                </Table>
                <Snackbar
                    open={this.state.openErrorSnackbar}
                    message={this.state.errorSnackbarMessage}
                    autoHideDuration={4000}
                />
            </div>
        );
    }
}
FeedTable.propTypes = {
    style: PropTypes.object,
    data: PropTypes.array.isRequired,
    showDetailsEventFunc: PropTypes.func.isRequired,
};