import React from 'react';
import {ProgressBox} from '../Misc/ProgressBox.jsx'
import {
    ComposedChart,
    ResponsiveContainer,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    Line
} from 'recharts';
import PropTypes from 'prop-types';

const InsertedBarChart = props => {
    
    const renderInsertedChart = (props) => {

        if (props.dataSource.progress) {
            const height = props.height+'px';
            return (<ProgressBox height={height}/>);
        }

        return (<ResponsiveContainer minHeight={props.height}>
            <ComposedChart
                style={{fontFamily: 'Lato, sans-serif', fontWeight: '400', fontSize: "12px"}}
                data={props.dataSource.data}>

                <XAxis dataKey="date"/>
                <Tooltip label="Legend"/>
                <CartesianGrid strokeDasharray="3 3"/>
                <Legend />
                <Line
                    type="monotone"
                    stackId="all"
                    name="Updated data"
                    dataKey="updated"
                    stroke='#AED581'
                    dot={{r: 6, fill: "white"}} strokeWidth={3}
                />
                <Line type="monotone"
                      stackId="all"
                      name="Added records"
                      dataKey="inserted"
                      stroke='#81C784'dot={{r: 6, fill: "white"}} strokeWidth={3}

                />
                <Line type="monotone"
                      stackId="all"
                      name="Deleted records"
                      dataKey="deleted"
                      stroke='#FFAB91'
                      dot={{r: 6, fill: "white"}} strokeWidth={3}
                />

            </ComposedChart>
        </ResponsiveContainer>);
    };
    
    return renderInsertedChart(props);
};

InsertedBarChart.propTypes = {
    height: PropTypes.number,
    dataSource: PropTypes.object.isRequired
};
InsertedBarChart.defaultProps = {
    height: '100%'
};

export default InsertedBarChart;