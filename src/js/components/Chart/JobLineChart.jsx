import React from 'react';
import {ProgressBox} from '../Misc/ProgressBox.jsx'
import {
    ComposedChart,
    ResponsiveContainer,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    Area
} from 'recharts';
import PropTypes from 'prop-types';

const JobLineChart = props => {
    
    const renderMainChart = (props) => {

        if (props.dataSource.progress) {
            const height = props.height+'px';
            return (<ProgressBox height={height}/>);
        }

        return (<ResponsiveContainer minHeight={props.height}>
            <ComposedChart
                style={{fontFamily: 'Lato, sans-serif', fontWeight: '400', fontSize: "12px"}}
                data={props.dataSource.data} margin={{top: 20, right: 20, left: 0, bottom: 0}}>
                <defs>
                    <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                        <stop offset="30%" stopColor="#888" stopOpacity={0.8}/>
                        <stop offset="75%" stopColor="#888" stopOpacity={0.4}/>
                    </linearGradient>
                </defs>
                <XAxis dataKey="date"/>
                <YAxis/>
                <CartesianGrid strokeDasharray="3 3"/>
                <Tooltip label="Legend"/>
                <Legend />

                <Area
                    dot={{r: 5, stroke: '#888', fill: "white"}}
                    type="monotone"
                    name="Jobs"
                    dataKey="count"
                    stroke="#888"
                    strokeWidth={4}
                    fillOpacity={0.9}
                    fill="#ccc"/>

            </ComposedChart>
        </ResponsiveContainer>);
    };
    
    return renderMainChart(props);
};

JobLineChart.propTypes = {
    height: PropTypes.number,
    dataSource: PropTypes.object.isRequired
};
JobLineChart.defaultProps = {
    height: '100%'
};

export default JobLineChart;