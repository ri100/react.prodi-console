import React from 'react';
import FeedCard from './FeedCard.jsx';
import ListOfFeedJobCards from './List/ListOfFeedJobCards.jsx'
import Switch from '@material-ui/core/Switch';
import Snackbar from '@material-ui/core/Snackbar';
import DivLink from './Misc/DivLink.jsx';

import {
    getRunningFeeds,
    getFeedCount,
    // getFailedFeeds,
    // getFeedsWithErrors,
    // getFinishedFeeds
} from "../actions/feedActions";

import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";

@connect((store) => {
    return {
        feedList: store.jobs,
        feedCount: store.filteredFeedCount,
        failedFeeds: store.failedFeeds,
    }
})
export default class JobList extends React.Component {

    interval = null;

    constructor(props) {
        super(props);
        this.state = {
            filter: this.getFilter(),
            openErrorMessage: false,
            panelType: 1,
        }
    }

    fetchData = (progress) => {
        this.props.dispatch(getRunningFeeds(this.getFilter(), progress));
        this.props.dispatch(getFeedCount(this.state.filter, true))
    };

    componentDidMount = () => {
        this.fetchData(true);
        this.runAutoRefresh(this.getToggle());
    };

    componentWillUnmount = () => {
        clearInterval(this.interval);
    };

    getToggle = () => {
        return localStorage.getItem('autoRefresh') ? JSON.parse(localStorage.getItem('autoRefresh')) : false;
    };

    setToggle = (toggle) => {
        localStorage.setItem('autoRefresh', JSON.stringify(toggle));
    };

    getFilter = () => {
        return localStorage.getItem('jobFilter') ? JSON.parse(localStorage.getItem('jobFilter')) : '';
    };

    setFilter = (filter) => {
        localStorage.setItem('jobFilter', JSON.stringify(filter));
    };

    autoRefresh = () => {
        this.props.dispatch(getRunningFeeds(this.getFilter(), false));
    };

    runAutoRefresh = (toggle) => {
        if (toggle) {
            this.interval = setInterval(this.autoRefresh, 2000);
        } else {
            clearInterval(this.interval);
        }
    };

    autoRefreshToggle = (e, toggle) => {
        this.setToggle(toggle);
        this.runAutoRefresh(toggle)
    };

    showErrorSnackbar = () => {
        return !(this.props.feedList.error == null && this.props.failedFeeds.error == null);
    };

    getErrorMessage = () => {

        if (this.props.feedList.error != null) {
            return 'Download progress: ' + this.props.feedList.error.message;
        }

        if (this.props.failedFeeds.error != null) {
            return 'Failed downloads: ' + this.props.failedFeeds.error.message;
        }

        return '';
    };

    renderFeeds = () => {
        if (this.props.feedList.progress === false) {
            return this.props.feedList.data.map((row, index) => (
                <FeedCard
                    key={index}
                    id={row._id}
                    active={(typeof row.process.active === "undefined") ? true : row.process.active}
                    jobId={(typeof row.process.job === "undefined" || typeof row.process.job.lastId === "undefined") ? '0' : row.process.job.lastId}
                    name={(typeof row.merchant === "undefined" || typeof row.merchant.name === "undefined") ? 'n/a' : row.merchant.name}
                    logo={(typeof row.merchant === "undefined" || typeof row.merchant.image === "undefined") ? null : row.merchant.image}
                    lastTime={(typeof row.download.date === "undefined") ? null : row.download.date}
                    deletedItems={(typeof row.parse === "undefined" || typeof row.parse.deleted === "undefined") ? null : row.parse.deleted}
                    tookTime={(typeof row.download.time === "undefined") ? null : row.download.time}
                    inventory={(typeof row.parse === "undefined" || typeof row.parse.matched === "undefined") ? null : row.parse.matched}
                    matchedItems={(typeof row.parse === "undefined" || typeof row.parse.matched === "undefined") ? null : row.parse.matched}
                    updatedItems={(typeof row.parse === "undefined" || typeof row.parse.updated === "undefined") ? null : row.parse.updated}
                    insertedItems={(typeof row.parse === "undefined" || typeof row.parse.inserted === "undefined") ? null : row.parse.inserted}
                    errors={(typeof row.parse === "undefined" || typeof row.parse.failed === "undefined") ? null : row.parse.failed}
                    parseProgress={(typeof row.parse === "undefined") ? null : row.parse.progress.value}
                    downloadProgress={(typeof row.download.status === "undefined" || typeof row.download.progress.value === "undefined") ? null : row.download.progress.value}
                    downloadStatus={(typeof row.download.status === "undefined" || typeof row.download.status.code === "undefined") ? null : row.download.status.code}
                    downloadSpeed={(typeof row.download.progress == 'undefined' || typeof row.download.progress.speed === "undefined") ? null : row.download.progress.speed}
                    parseStatus={(typeof row.parse === "undefined") ? null : row.parse.status.code}
                    progressType={(typeof row.download.progress == 'undefined' || typeof row.download.progress.type === "undefined") ? null : row.download.progress.type}
                    status={(typeof row.parse == "undefined" || row.process.status === 'undefined' || row.process.status.code === 'undefined') ? null : row.process.status.code}
                    statusMessage={(typeof row.parse == "undefined" || row.process.status === 'undefined' || row.process.status.message === 'undefined') ? null : row.process.status.message}
                    autoUpdate={true}
                />
            ))
        }
    };

    renderPanel = () => {
        return <ListOfFeedJobCards
            displayItemHandler={this.renderFeeds}
            progress={this.props.feedList.progress}
            height="100%"
        />
    };

    renderHeaderLabel = () => {
        let label = "Feed jobs";
        return <div style={{padding: '15px 5px 5px 25px', fontSize: '300%', fontWeight: '300'}}>
            {label}
        </div>
    };

    renderCount = () => {
        if (this.props.feedCount.progress === false) {
            return this.props.feedCount.data
        }
    }


    render() {
        return (
            <div className="dataPane">

                <header className="jobStatusHeader">

                    <div className="flexDiv">
                        <div className="jobStatusLeft">
                            {this.renderHeaderLabel()}
                        </div>
                        <div className="jobStatusRight">
                            <Switch
                                defaultChecked={this.getToggle()}
                                onChange={this.autoRefreshToggle}
                            />
                        </div>
                    </div>

                    <div style={{padding: '0 15px 0 25px'}}>
                        <DivLink
                            defaultSelected={this.state.panelType === 1}
                            onClick={() => {
                                this.setState({panelType: 1});
                                this.fetchData(true);
                            }}>Status</DivLink>
                    </div>
                </header>

                <div className="jobFilter">
                    <TextField value={this.state.filter}
                               label="Filter jobs"
                               style={{width: "calc(100% - 110px)"}}
                               onChange={(ev) => {
                                   this.setState({filter: ev.target.value});
                               }}
                               onKeyPress={(ev) => {
                                   if (ev.key === 'Enter') {
                                       this.setFilter(this.state.filter);
                                       this.props.dispatch(getRunningFeeds(this.state.filter, true));
                                       this.props.dispatch(getFeedCount(this.state.filter, true))
                                       ev.preventDefault();
                                   }
                               }}/>
                    <div className="feedCounter">
                        <div className="title">Found:</div>
                        <div className="value">{this.renderCount()}</div>
                    </div>
                </div>
                <div className="jobStatusData">
                    {this.renderPanel()}
                </div>
                <Snackbar
                    open={this.showErrorSnackbar()}
                    message={this.getErrorMessage()}
                    autoHideDuration={4000}
                />

            </div>
        );
    }
}