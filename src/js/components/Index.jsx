import React from 'react';
import {IndexRoute, Router, Route, hashHistory, browserHistory} from "react-router"
import JobList from './JobList.jsx';
import FeedDetails from './FeedDetails.jsx';
import FeedConsole from './FeedConsole.jsx';
import ProviderDetails from './ProviderDetails.jsx';
import Dashboard from './Dashboard.jsx';
import ProviderList from './ProviderList.jsx';
import FeedList from './FeedList.jsx';
import App from "./App.jsx";
import OfferSearch from './OfferSearch.jsx';
import AllLogConsole from './AllLogConsole.jsx';
import SignIn from './Dialog/SignIn.jsx';

export default class Index extends React.Component {
    render() {
        return (
            <Router history={hashHistory}>
                <Route path="/auth" component={SignIn} />
                <Route path="/" component={App}>
                    <IndexRoute component={Dashboard}/>
                    <Route path="/console" component={AllLogConsole}/>
                    <Route path="/offers" component={OfferSearch}/>
                    <Route path="/jobList" component={JobList}/>
                    <Route path="/feedList" component={FeedList} />
                    <Route path="/feedDetails/:feedId/:jobId" component={FeedDetails}/>
                    <Route path="/jobConsole/:feedId/:jobId" component={FeedConsole}/>
                    <Route path="/providers" component={ProviderList}/>
                    <Route path="/providerDetails/:id" component={ProviderDetails}/>
                </Route>
            </Router>
        );
    };
}