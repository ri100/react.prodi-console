export default function(state={ data: [], error: null, progress: true }, action) {
    switch (action.type) {
        case "FEEDS_WITH_ERRORS_LOADING":
            return {
                ...state,
                data:[],
                error: null,
                progress: true
            };
        case "FEEDS_WITH_ERRORS_LOADED":
            return {
                ...state,
                data: action.data,
                error: null,
                progress: false
            };
        case "FEEDS_WITH_ERRORS_FAILED":
            return {
                ...state,
                data: [],
                error: action.error,
                progress: true
            };
    }

    return state;
}