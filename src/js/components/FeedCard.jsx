import React from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import CircularProgress from '@material-ui/core/CircularProgress';
import FeedStats from './FeedStats.jsx';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ConsoleIcon from "./Dialog/ConsoleIcon.jsx";
import ProgressLogConsole from "./ProgressLogConsole.jsx";
import DialogMsg from "./Dialog/DialogMsg.jsx";
import { runFeed } from '../repo/feedDataApi.js'
import "./FeedCard.css"
import {CloudDownload} from '@material-ui/icons';

const WhiteCircularProgress = withStyles({
    root: {
        color: 'white',
    },
})(CircularProgress);


export default class FeedCard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            update_icon: "cloud_download"
        };
    }

    getBackgroundColor = () => {

        let lastUpdateTime = new Date(this.props.lastTime).getTime();
        let now = new Date().getTime();
        let isLagging = now - lastUpdateTime > 10 * 1000;
        let isStale = now - lastUpdateTime > 120 * 1000;

        let stale = "#4a148c";
        let lagging = "#0d47a1";
        let green = '#2e7d32';
        let orange = '#ff4d00';
        let red = '#ee0000';
        let dark_green = '#1b5e20';
        let light_gray = '#546e7a';
        let blue = 'rgb(21, 101, 192)';
        if (this.props.active === false) {
            return light_gray;
        } else if ((this.props.status === 'started' || this.props.status === 'parsing' || this.props.status === 'downloading') && isStale) {
            return stale;
        } else if ((this.props.status === 'started' || this.props.status === 'parsing' || this.props.status === 'downloading') && isLagging) {
            return lagging;
        } else if (this.props.status === 'failed') {
            return orange;
        } else if (this.props.status === 'finished') {
            return green;
        } else if (this.props.status === 'stopped') {
            return dark_green;
        } else if (this.props.status === 'internal-error') {
            return red;
        } else {
            return blue;
        }
    };

    getColor = () => {
        let white = '#fff';
        if (this.props.status === 'failed') {
            return white;
        } else if (this.props.status === 'finished') {
            return white;
        } else {
            return white;
        }
    };

    displaySyncingCard = () => {
        return (
            <div className="bottomInfo">
                <div className="downloading">Syncing with cache</div>
                <div className="progressContainer">
                    <WhiteCircularProgress />
                </div>
            </div>)

    };

    displayDownloadContent = (type) => {
        if (type === 'percentage') {
            return (
                <div className="bottomInfo">
                    <div className="downloading">Downloading: {this.props.downloadSpeed} kb/s</div>
                    <div className="progressContainer">
                        <WhiteCircularProgress variant="determinate" value={this.props.downloadProgress} />
                    </div>
                    <div className="downloadProgressValue">
                        Progress: <span style={{ fontWeight: 400 }}>{this.props.downloadProgress}</span>%
                    </div>
                </div>)

        } else if (type === 'file-size') {
            return (
                <div className="bottomInfo">
                    <div className="downloading">Downloading: {this.props.downloadSpeed} kb/s</div>
                    <div className="progressContainer">
                        <WhiteCircularProgress /><br />
                    </div>
                    <div className="downloadProgressValue">
                        Downloaded: <span style={{ fontWeight: 400 }}>{this.props.downloadProgress}</span> kb
                    </div>
                </div>)
        }

        return (
            <div className="bottomInfo">
                <div className="downloading">Connecting...</div>
                <div className="progressContainer">
                    <WhiteCircularProgress />
                </div>
                <div className="downloadProgressValue">
                    Please wait.
                </div>
            </div>)

    };

    displayParseContent = () => {
        return (<div className="bottomInfo">
            {this.displayProgress()}
            <FeedStats color={this.getColor()}
                lastTime={this.props.lastTime}
                tookTime={this.props.tookTime}
                status={this.props.status}
                statusMsg={this.props.statusMessage + " [Download: " + this.props.downloadStatus + ", Parse: " + this.props.parseStatus + "]"}
                inventory={this.props.matchedItems + this.props.errors}
                matchedItems={this.props.matchedItems}
                deletedItems={this.props.deletedItems}
                insertedItems={this.props.insertedItems}
                updatedItems={this.props.updatedItems}
                errors={this.props.errors}
            />
        </div>);
    };

    displayContent = () => {
        if (this.props.status === 'downloading') {
            return this.displayDownloadContent(this.props.progressType);
        } else if (this.props.status === 'syncing') {
            return this.displaySyncingCard();
        }
        return this.displayParseContent();
    };

    displayProgress = () => {
        if (this.props.status === 'parsing') {
            return <LinearProgress variant="determinate" style={{ margin: "0" }} color="secondary"
                value={this.props.parseProgress} />
        } else {
            return <div style={{ height: "4px" }}></div>
        }
    };

    handleFeedUpdate = () => {
        runFeed(this.props.id,
            () => {

            },
            (error) => {
            }
        );
    };

    handleUpdateClick = () => {
        this.dialog.setState({
            openDialog: true
        });
    };

    render() {
        return (
            <div className="FeedCard" style={{
                backgroundColor: this.getBackgroundColor(),
                transition: "all .4s ease-in"
            }}>
                <div className="merchantName" title={this.props.name}>
                    <div className="merchantTitle">{this.props.name}</div>
                    <CloudDownload style={{ height: '23px', cursor: "pointer", margin: "0 0 4px 5px" }}
                                   onClickCapture={this.handleUpdateClick}/>

                    <DialogMsg title="Please confirm"
                        onOK={this.handleFeedUpdate}
                        open={false}
                        OKLabel="Yes"
                        CancelLabel="No"
                        ref={(dialog) => {
                            this.dialog = dialog;
                        }}>Do you want to update feed {this.props.name} ?</DialogMsg>
                    <ConsoleIcon
                        label="Console"
                        title="Logs"
                        OKLabel="Close">
                        <ProgressLogConsole jobId={this.props.jobId} />
                    </ConsoleIcon>
                </div>
                <Link to={"/feedDetails/" + this.props.id + "/" + this.props.jobId} onlyActiveOnIndex
                    className="jobCardLink">
                    {this.displayContent()}
                </Link>
            </div>

        );
    }
}

FeedCard.propTypes = {
    active: PropTypes.bool.isRequired,
    status: PropTypes.string.isRequired,
    statusMessage: PropTypes.string,
    downloadStatus: PropTypes.string.isRequired,
    downloadSpeed: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]).isRequired,
    parseStatus: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    jobId: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    lastTime: PropTypes.string.isRequired,
    tookTime: PropTypes.number.isRequired,
    logo: PropTypes.string,
    inventory: PropTypes.number,
    matchedItems: PropTypes.number,
    updatedItems: PropTypes.number,
    deletedItems: PropTypes.number,
    insertedItems: PropTypes.number,
    errors: PropTypes.number,
    parseProgress: PropTypes.number,
    downloadProgress: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    progressType: PropTypes.string,
    autoUpdate: PropTypes.bool,
};
FeedCard.defaultProps = {
    logo: null,
    parseProgress: 0,
    downloadProgress: 0,
    autoUpdate: false,
    errors: 0,
    progressType: 'file-size',
    onProgressComplete: null,
    matchedItems: null,
    updatedItems: null,
    deletedItems: null,
    insertedItems: null,
    inventory: null,
};
