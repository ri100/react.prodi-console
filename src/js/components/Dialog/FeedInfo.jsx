import React from 'react';
import {ProcessStatusBox, DownloadStatusBox, ParseStatusBox} from '../Misc/StatusBox.jsx';
import PropTypes from "prop-types";
import { getDetails } from '../../repo/feedDataApi.js'
import {ProgressLine} from "../Misc/ProgressLine.jsx";

export default class FeedInfo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: props.feedData.data
        }
    }

    componentDidMount() {
        this.interval = setInterval(this.refreshFeedData, 2000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    refreshFeedData = () => {
        getDetails(
            this.props.id,
            (response) => {
                this.setState({
                    data: response.data
                })
            },
            (response) => {
                clearInterval(this.interval);
            }
        );
    }

    render() {
        return <div style={{display: "flex", padding: "20px", flexWrap: "wrap"}}>
            <div className="FeedCardInfoBox">
                <h2>Provider data</h2>
                <table>
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{this.state.data.merchant.name}</td>
                    </tr>
                    <tr>
                        <td>Url</td>
                        <td>{this.state.data.download.url}</td>
                    </tr>
                    <tr>
                        <td>Schema</td>
                        <td>{this.state.data.download.schema}</td>
                    </tr>
                    <tr>
                        <td>Hash</td>
                        <td>{this.state.data.merchant.hash}</td>
                    </tr>
                    <tr>
                        <td>Active</td>
                        <td>{this.state.data.process.active ? "yes" : "no"}</td>
                    </tr>
                    </tbody>
                    <tr>
                        <td>Last job id</td>
                        <td>{this.state.data.process.job.lastId}</td>
                    </tr>
                    <tr>
                        <td>Last job time</td>
                        <td>{this.state.data.process.job.startTime}</td>
                    </tr>
                    <tr>
                        <td>Activity date</td>
                        <td>{this.state.data.download.date}</td>
                    </tr>
                </table>
            </div>
            <div className="FeedCardInfoBox">
                <h2>Download information</h2>
                <table>
                    <tbody>

                    <tr>
                        <td>File size</td>
                        <td>{this.state.data.download.size}</td>
                    </tr>
                    <tr>
                        <td>Download speed</td>
                        <td>{this.state.data.download.progress.speed} kb/s</td>
                    </tr>
                    <tr>
                        <td>Download time</td>
                        <td>{this.state.data.download.progress.time.toFixed(2)} s</td>
                    </tr>
                    <tr>
                        <td>Download progress</td>
                        <td>{this.state.data.download.progress.value}</td>
                    </tr>

                    <tr>
                        <td>Status</td>
                        <td><DownloadStatusBox status={this.state.data.download.status.code}/></td>
                    </tr>
                    <tr>
                        <td>Message</td>
                        <td>{this.state.data.download.status.message}</td>
                    </tr>

                    </tbody>
                </table>

            </div>
            <div className="FeedCardInfoBox">
                <h2>Parse information</h2>
                <table>
                    <tbody>
                    <tr>
                        <td>Status</td>
                        <td><ParseStatusBox status={this.state.data.parse.status.code}/></td>
                    </tr>
                    <tr>
                        <td>Message</td>
                        <td>{this.state.data.parse.status.message}</td>
                    </tr>
                    <tr>
                        <td>Parse time</td>
                        <td>{this.state.data.parse.progress.time.toFixed(2)} s</td>
                    </tr>
                    <tr>
                        <td>Parse speed</td>
                        <td>{this.state.data.parse.progress.speed} items/s</td>
                    </tr>
                    <tr>
                        <td>Parse progress</td>
                        <td><ProgressLine>{this.state.data.parse.progress.value}</ProgressLine></td>
                    </tr>
                    <tr>
                        <td>Tidy</td>
                        <td>{this.state.data.parse.tidy ? "yes" : "no"}</td>
                    </tr>
                    <tr>
                        <td>Matched</td>
                        <td>{this.state.data.parse.matched} items</td>
                    </tr>
                    <tr>
                        <td>Inserted</td>
                        <td>{this.state.data.parse.inserted} items</td>
                    </tr>
                    <tr>
                        <td>Updated</td>
                        <td>{this.state.data.parse.updated} items</td>
                    </tr>
                    <tr>
                        <td>Deleted</td>
                        <td>{this.state.data.parse.deleted} items</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div className="FeedCardInfoBox">
                <h2>Process information</h2>
                <table>
                    <tbody>
                    <tr>
                        <td>Status</td>
                        <td><ProcessStatusBox status={this.state.data.process.status.code}/></td>
                    </tr>
                    <tr>
                        <td>Message</td>
                        <td>{this.state.data.process.status.message}</td>
                    </tr>
                    <tr>
                        <td>Process time</td>
                        <td>{(this.state.data.download.progress.time + this.state.data.parse.progress.time).toFixed(2)} s</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    };
};
FeedInfo.propTypes = {
    id: PropTypes.string.isRequired,
    feedData: PropTypes.object.isRequired,
}