# Instalacja

```
sudo apt update
sudo apt install curl git
```

## Instalacja prodi-console
```
cd /opt
git clone https://ri100@bitbucket.org/ri100/react.prodi-console.git
```

## Instalacja nodejs

```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt install nodejs
nodejs --version
```

#aktualizacja listy browserwó
```
npx browserslist@latest --update-db
```
Run npx browserslist in project directory to see what browsers was selected by your queries.

#Instalacja webpack
```
cd /opt/react.prodi-console
npm install -g yarn
```

##Uruchom webpack
```
yarn prod
```

Otrzymasz plik bundle.js

##Uruchom

browser http://prodi.console

# Tworzenie docker image

```
docker build -t prodi-console .
```

# Uruchamianie docker image

```
docker run -p 8010:80 prodi-console
```

wejdz na localhost:8010

