import React from 'react';
import {ProgressBox} from '../Misc/ProgressBox.jsx'
import {
    ComposedChart,
    ResponsiveContainer,
    XAxis,
    Tooltip,
    Legend,
    Line
} from 'recharts';
import PropTypes from 'prop-types';

const AvgTimeLineChart = props => {
    const renderAvgTimeChart = (props) => {

        if (props.dataSource.progress) {
            const height = props.height+'px';
            return (<ProgressBox height={height}/>);
        }

        return (<ResponsiveContainer minHeight={props.height}>
            <ComposedChart
                style={{fontFamily: 'Lato, sans-serif', fontWeight: '400', fontSize: "12px"}}
                data={props.dataSource.data}>

                <XAxis dataKey="date"/>
                <Tooltip label="Legend"/>
                <Line
                    type="linear"
                    name="Avg Time spend running"
                    dataKey="avgTime"
                    fill='#90A4AE'
                    dot={{r: 6, stroke: '#90A4AE', fill: "white"}} strokeWidth={3}
                />
                <Legend />
            </ComposedChart>
        </ResponsiveContainer>);
    };
    
    return renderAvgTimeChart(props);
};

AvgTimeLineChart.propTypes = {
    height: PropTypes.number,
    dataSource: PropTypes.object.isRequired
};
AvgTimeLineChart.defaultProps = {
    height: '100%'
};

export default AvgTimeLineChart;