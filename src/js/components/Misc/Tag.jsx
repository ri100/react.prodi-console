import React from 'react';
import PropTypes from 'prop-types';

export const Tag = props => {
    
    const style = {
        padding: "6px 9px", 
        backgroundColor: props.backgroundColor, 
        color:  props.color, 
        borderRadius: "3px",
        textTransform:'uppercase'
    };

    return <span style={style} title={props.title}>{props.children}</span>
};

Tag.propTypes = {
    backgroundColor: PropTypes.string,
    color: PropTypes.string,
    title: PropTypes.string
};

Tag.defaultProps = {
    backgroundColor: 'red',
    color: 'white',
    title: ''
};