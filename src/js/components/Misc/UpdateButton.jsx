import React from 'react';
import PropTypes from 'prop-types';

const UpdateButton = (props) => {

    const style = {
        textTransform: 'uppercase',
        borderRadius: '4px',
        border: 'solid white 1px',
        verticalAlign: 'middle',
        marginLeft: props.marginLeft,
        color: 'white',
        fontSize: '70%',
        fontWeight: 400,
        cursor: 'pointer',
        padding: '5px',
        backgroundColor: props.backgroundColor
    };

    return <span
        onClick={()=>{props.onClick()}}
        style={style}>{props.children}</span>
};

UpdateButton.propTypes = {
    onClick: PropTypes.func.isRequired
};
UpdateButton.defaultProps = {
    backgroundColor: 'transparent',
    marginLeft: '15px'
};

export default UpdateButton;