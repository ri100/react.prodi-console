import React from 'react';
import PropTypes from 'prop-types';

export default class FeedData extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (<div className="providerData">
            <div title={this.props.downloadUrl} className="header" style={{backgroundColor: this.props.backgroundColor, color: this.props.color}}>
                <h1 className="name">{this.props.name}</h1>
                <h2 className="url">{this.props.url}</h2>
            </div>
            <div className="infoDiv">
                <table className="contact">
                    <tbody>
                    <tr>
                        <td>Hash</td>
                        <td>{this.props.hash}</td>
                    </tr>
                    <tr>
                        <td>Schema</td>
                        <td>{this.props.schema}</td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td>{this.props.status}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>);
    }
}

FeedData.propTypes = {
    name: PropTypes.string.isRequired,
    hash: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    downloadUrl: PropTypes.string.isRequired,
    schema: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired
};
FeedData.defaultProps = {
    backgroundColor: 'rgb(63, 81, 181)',
    color: 'white'
};
