import React from 'react';
import DialogMsg from './DialogMsg.jsx';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon'
import PropTypes from 'prop-types';


export default class ConfirmationButton extends React.Component {

    constructor(props) {
        super(props);

        if (this.props.icon) {
            this.icon = <Icon className="material-icons">{this.props.icon}</Icon>;
        }

    }

    handleClick = () => {
        this.dialog.setState({
            openDialog: true
        });
    };

    render() {
        return (
            <span style={{display: "inline"}}>
                <Button
                    variant="contained"
                    // labelPosition={this.props.labelPosition}
                    icon={this.icon}
                    disabled={this.props.disabled}
                    primary={this.props.primary}
                    secondary={this.props.secondary}
                    onClick={this.handleClick}
                    style={this.props.style}
                    // backgroundColor={this.props.backgroundColor}
                >
                    {this.props.label}
                </Button>
                 <DialogMsg title={this.props.title}
                            onOK={this.props.onOK}
                            open={false}
                            OKLabel={this.props.OKLabel}
                            CancelLabel={this.props.CancelLabel}
                            ref={(dialog) => {
                                this.dialog = dialog;
                            }}>
                     {this.props.children}
                 </DialogMsg>
            </span>
        );
    }
}

ConfirmationButton.propTypes = {
    disabled: PropTypes.bool,
    secondary: PropTypes.bool,
    primary: PropTypes.bool,
    icon: PropTypes.string,
    label: PropTypes.string.isRequired,
    // labelPosition: PropTypes.string,
    title: PropTypes.string.isRequired,
    onOK: PropTypes.func.isRequired,
    OKLabel: PropTypes.string,
    CancelLabel: PropTypes.string
};
ConfirmationButton.defaultProps = {
    disabled: false,
    secondary: null,
    icon: null,
    style: null,
    primary: null,
    // labelPosition: 'before',
    OKLabel: 'OK',
    CancelLabel: 'Cancel',
    // backgroundColor: 'white'
};
