import { combineReducers } from "redux"

import jobReducer from "./jobReducer"
import failedJobs from "./failedJobsReducer"
import finishedJobs from "./finishedJobsReducer"
import feedLogReducer from "./feedLogReducer"
import processLogReducer from "./progressLogReducer"
import processLogsReducer from "./progressLogsReducer"
import errorLogReducer from "./errorLogReducer"
import jobChartDataReducer from "./jobChartDataReducer"
import feedDetails from "./feedDetailsReducer"
import feedList from "./feedListReducer"
import feedCount from "./feedCountReducer"
import providerJobsChartData from "./merchantJobsChartData"
import providerSearch from './providerSearchReducer'
import providerDetails from './providerDetailsReducer'
import dashboardChart from './dashboardChartReducer'
import dashboardCache from './dashboardCacheReducer'
import feedsWithErrors from './feedsWithErrors'
import feedsStats from './feedsStats'
import feedTable from './feedTable'
import searchReducer from "./searchReducer";

export default combineReducers({
    jobs: jobReducer,
    failedFeeds: failedJobs,
    finishedFeeds: finishedJobs,
    feedLog: feedLogReducer,
    feedDetails: feedDetails,
    sourceList: feedList,
    filteredFeedCount: feedCount,
    progressLog: processLogReducer,
    progressLogs: processLogsReducer,
    errorLog: errorLogReducer,
    jobChartData: jobChartDataReducer,
    providerJobsChartData: providerJobsChartData,
    providersList: providerSearch,
    providerDetails: providerDetails,
    dashboardChart: dashboardChart,
    dashboardCache: dashboardCache,
    feedsWithErrors: feedsWithErrors,
    feedsStats: feedsStats,
    feedTable: feedTable,
    search: searchReducer
});
