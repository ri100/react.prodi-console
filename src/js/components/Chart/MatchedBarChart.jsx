import React from 'react';
import {ProgressBox} from '../Misc/ProgressBox.jsx'
import {
    ComposedChart,
    ResponsiveContainer,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    Bar
} from 'recharts';
import PropTypes from 'prop-types';

const MatchedBarChart = props => {
    const renderMatchedChart = (props) => {

        if (props.dataSource.progress) {
            const height = props.height+'px';
            return (<ProgressBox height={height}/>);
        }

        return (<ResponsiveContainer minHeight={props.height}>
            <ComposedChart
                style={{fontFamily: 'Lato, sans-serif', fontWeight: '400', fontSize: "12px"}}
                data={props.dataSource.data}>

                <XAxis dataKey="date"/>
                <Tooltip label="Legend"/>
                <Bar stackId="all"
                     name="Retrieved records"
                     dataKey="matched"
                     fill='#90CAF9'
                />
                <Bar stackId="all"
                     name="Failed records"
                     dataKey="failed"
                     fill='#FFAB91'
                />
                <Legend />

            </ComposedChart>
        </ResponsiveContainer>);
    };
    
    return renderMatchedChart(props);
};

MatchedBarChart.propTypes = {
    height: PropTypes.number,
    dataSource: PropTypes.object.isRequired
};
MatchedBarChart.defaultProps = {
    height: '100%'
};

export default MatchedBarChart;