import axios from 'axios';

export function loadJson(url, params, onSuccess, onError) {
    if(url.substring(0,4) !== 'http') {
        url =  process.env.API_URL + url;
    }
    axios.post(url, params).then((response) => {
        onSuccess(response);
    }).catch((error) => {
        onError(error)
    });
}