import React from 'react';
import {ProgressBox} from './Misc/ProgressBox.jsx';
import PropTypes from 'prop-types';

export default class ErrorConsole extends React.Component {
    
    displayContent = () => {
        if(this.props.loading) {
            return (
                <ProgressBox height="100%"/>
            );
        }
        return this.props.logLines.map((row, index) => (
                <div key={index}>
                    TYPE: {row.error.type}<br />
                    NODE: {row.error.node}<br />
                    MESSAGE: {row.error.message}<br />
                    DATA: {row.error.data}<br /><br />
                </div>
            ))
    };
    
    render() {
        return(
            <div className="console">
                {
                    this.displayContent()
                }
            </div>
        );
    }
}
ErrorConsole.propTypes = {
    logLines: PropTypes.array.isRequired,
};

ErrorConsole.defaultProps = {
    loading: true
};