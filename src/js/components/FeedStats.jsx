import React from 'react';
import PropTypes from 'prop-types';

export default class FeedStats extends React.Component {

    constructor(props) {
        super(props);
    }
    
    render() {
        return (<div style={{color: this.props.color}} className="downloadStats">
            <div className="lastDownloadInfo">
                Last update at: <span className="value">{this.props.lastTime}</span><br/>
                Status: <span className="value" title={this.props.statusMsg}>{this.props.status}</span>, Took: <span className="value">{this.props.tookTime} sec.</span> <br/>
                <table style={{width:"100%", margin: 3}} className="stats">
                    <tbody>
                        <tr>
                            <td className="name">Matched</td><td> {this.props.matchedItems}</td>
                            <td className="name">Inventory</td><td>{this.props.inventory}</td>
                        </tr><tr>
                            <td className="name">Deleted</td><td>{this.props.deletedItems}</td>
                            <td className="name">Errors</td><td>{this.props.errors}</td>
                        </tr><tr>
                            <td className="name">Inserted</td><td>{this.props.insertedItems}</td>
                            <td></td>
                        </tr><tr>
                            <td className="name">Updated</td><td>{this.props.updatedItems}</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>  );
};
    
}

FeedStats.propTypes = {
    lastTime: PropTypes.string.isRequired,
    tookTime: PropTypes.number.isRequired,
    status: PropTypes.string.isRequired,
    statusMsg: PropTypes.string.isRequired,
    logo: PropTypes.string,
    inventory: PropTypes.number.isRequired,
    matchedItems: PropTypes.number.isRequired,
    updatedItems: PropTypes.number.isRequired,
    deletedItems: PropTypes.number.isRequired,
    insertedItems: PropTypes.number.isRequired,
};
FeedStats.defaultProps = {
    logo: 'NoLogo',
    errors: 0,
    color: 'rgb(63, 81, 181)'
};