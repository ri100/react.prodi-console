const path = require('path');
const webpack = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
    mode: 'production',
    entry: './src/js/index.jsx',
    optimization: {
        minimizer: [new TerserPlugin({ /* additional options here */ })],
    },
    output: { path: __dirname, filename: 'public/bundle.js' },
    module: {
        rules: [
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: ["@babel/preset-env", "@babel/preset-react"],
                    plugins: [
                        ["@babel/plugin-proposal-decorators", { "legacy": true }],
                        "@babel/plugin-proposal-class-properties",
                        "@babel/transform-arrow-functions",
                    ]
                }
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env':{
                'API_URL': JSON.stringify("http://api.dashboard.kuptoo.pl:18888/")
            }
        })
    ],
    devServer: {
        watchOptions: {
            ignored: [
                path.resolve(__dirname, '.idea'),
                path.resolve(__dirname, '.vscode'),
                path.resolve(__dirname, 'dist'),
                path.resolve(__dirname, 'node_modules')
            ]
        }
    },
};