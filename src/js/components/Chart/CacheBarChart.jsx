import React from 'react'
import {ProgressBox} from '../Misc/ProgressBox.jsx'
import {
    ComposedChart,
    ResponsiveContainer,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    Bar
} from 'recharts';
import PropTypes from 'prop-types';

const CacheBarChart = props => {

   const renderChart = (props) => {

       if (props.dataSource.progress) {
           var height = props.height+'px';
           return (<ProgressBox height={height}/>);
       }

        return (<ResponsiveContainer minHeight={props.height}>
            <ComposedChart
                style={{fontFamily: 'Lato, sans-serif', fontWeight: '400', fontSize: "12px"}}
                data={props.dataSource.data}>

                <XAxis dataKey="date"/>
                <Tooltip label="Legend"/>

                <Bar
                    name="unSyncDatabase data"
                    dataKey="unSyncDatabaseData"
                    fill='#AED581'
                />
                <Bar
                    name="unSyncCache data"
                    dataKey="unSyncCacheData"
                    fill='#FFAB91'
                />

                <Legend />

            </ComposedChart>
        </ResponsiveContainer>);
    };

    return renderChart(props);
};

CacheBarChart.propTypes = {
    height: PropTypes.number,
    dataSource: PropTypes.object.isRequired
};
CacheBarChart.defaultProps = {
    height: '100%'
};

export default CacheBarChart;