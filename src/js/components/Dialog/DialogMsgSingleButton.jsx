import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';

export default class DialogMsgSingleButton extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            openDialog: this.props.open
        };
    }

    confirm = () => this.setState({openDialog: false});

    render() {
        return (
            <Dialog open={this.state.openDialog} maxWidth={"md"}>
                <DialogTitle id="form-dialog-title" style={{padding: '6px 24px'}}>{this.props.title}</DialogTitle>
                <DialogContent style={{padding:0}}>
                    <DialogContentText style={{margin:0}}>
                        {this.props.children}
                    </DialogContentText>
                </DialogContent>
                <DialogActions style={{padding: '6px 24px'}}>
                    <Button onClick={this.confirm} color="primary">
                        {this.props.OKLabel}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

DialogMsgSingleButton.propTypes = {
    open: PropTypes.bool.isRequired,
    title: PropTypes.string,
    OKLabel: PropTypes.string,
};

DialogMsgSingleButton.defaultProps = {
    OKLabel: 'Close',
};
