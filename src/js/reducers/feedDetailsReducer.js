export default function(state={ data: {}, error: null, progress: true }, action) {
    switch (action.type) {
        case "SOURCE_DETAILS_LOADING":
            return {
                ...state,
                data:{},
                error: null,
                progress: true
            };
        case "SOURCE_DETAILS_LOADED":
            return {
                ...state,
                data: action.data,
                error: null,
                progress: false
            };
        case "SOURCE_DETAILS_FAILED":
            return {
                ...state,
                data: {},
                error: action.error,
                progress: true
            };
    }

    return state;
}