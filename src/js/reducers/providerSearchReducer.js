export default function(state={ data: [], error: null, progress: true }, action) {
    switch (action.type) {
        case "PROVIDER_SEARCHING_LOADING":
            return {
                ...state,
                data:[],
                error: null,
                progress: true
            };
        case "PROVIDER_SEARCHING_LOADED":
            return {
                ...state,
                data: action.data,
                error: null,
                progress: false
            };
        case "PROVIDER_SEARCHING_FAILED":
            return {
                ...state,
                data: [],
                error: action.error,
                progress: true
            };
    }

    return state;
}