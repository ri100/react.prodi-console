import React from 'react';
import PropTypes from 'prop-types';

export default class DivLink extends React.Component {

    notSelected = {
        display: 'inline-block',
        textTransform: 'uppercase',
        marginRight: '20px',
        fontSize: '90%',
        color: this.props.notSelectedColor,
        height: '24px',
        cursor: 'pointer'
    };

    selected = {
        display: 'inline-block',
        textTransform: 'uppercase',
        marginRight: '20px',
        fontSize: '90%',
        color:  this.props.color,
        height: '24px',
        borderBottom: 'solid ' + this.props.color + ' 3px'
    };
    
    getTabStyle = () => {
        return this.props.defaultSelected ? this.selected : this.notSelected
    };
    
    render() {
        return(
            <div style={this.getTabStyle()} onClick={this.props.onClick}>{this.props.children}</div>
        );
    }
}

DivLink.propTypes = {
    onClick: PropTypes.func,
    defaultSelected: PropTypes.bool.isRequired,
    color: PropTypes.string,
    notSelectedColor: PropTypes.string,
};

DivLink.defaultProps = {
    color: "white",
    notSelectedColor: "#c0cccc"
};