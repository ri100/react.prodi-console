import React from 'react';
import Paper from '@material-ui/core/Paper';
import Snackbar from '@material-ui/core/Snackbar';
import {Link} from 'react-router';
import ProviderData from './ProviderData.jsx';
import ConfirmationButton from './Dialog/ConfirmationButton.jsx';
import { runAllFeedsFromProvider } from '../repo/providerDataApi.js'
import PropTypes from 'prop-types';

export default class ProviderCard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            openSnackbar: false
        }
    }

    updateData = () => {
        runAllFeedsFromProvider(
            this.props.id,
            () => {
                this.setState({
                    openSnackbar: true
                });
            },
            (error) => {
                console.log(error.message);
            }
        );

    };

    handleClose = (event, reason) => {
        console.log(reason);
        this.setState({
            openSnackbar: false
        });
    };

    render() {
        return (
            <Paper className="providerCard" elevation={2}>
                <Link to={"/providerDetails/" + this.props.id} onlyActiveOnIndex>
                    <ProviderData
                        name={this.props.name}
                        url={this.props.url}
                        street={this.props.street}
                        town={this.props.town}
                        phone={this.props.phone}
                        email={this.props.email}
                    />
                </Link>
                <div style={{width: "100%", height: "50px", display: "table", borderTop: "solid 1px #e5e5e5", padding: "7px", boxSizing: "border-box"}}>
                    <div style={{width: "50%", display: "table-cell", textAlign:"right", verticalAlign:"middle"}}>
                        <ConfirmationButton label="Update"
                                            onOK={this.updateData}
                                            title="Confirm"
                                            OKLabel="Yes"
                                            CancelLabel="No">
                            Do you really want to update data from this provider?
                        </ConfirmationButton>

                    </div>
                </div>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    open={this.state.openSnackbar}
                    onClose={this.handleClose}
                    autoHideDuration={2000}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">Job queued</span>}
                />
            </Paper>
        );
    }
}

ProviderCard.propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    street: PropTypes.string,
    town: PropTypes.string,
    email: PropTypes.string,
    phone: PropTypes.string,
    active: PropTypes.bool.isRequired,
};
ProviderCard.defaultProps = {
    street: 'n/a',
    town: 'n/a',
    email: 'n/a',
    phone: 'n/a'
};
