import React from 'react';
import FeedTable from './Table/FeedTable.jsx'
import {connect} from "react-redux";
import {getNotMatched} from '../actions/feedActions';

@connect((store) => {
    return {
        feedTable: store.feedTable
    }
})
export default class FeedList extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount = () => {
      this.props.dispatch(getNotMatched());  
    };
    
    render() {
        return <FeedTable data={this.props.feedTable.data} showDetailsEventFunc={() => {}} />
    }
}