import React from 'react';
import { ProgressBox } from '../Misc/ProgressBox.jsx';
import PropTypes from 'prop-types';
import "./ListOfFeedJobCards.css";

export default class ListOfFeedJobCards extends React.Component {

    displayContent= (progress) => {
        if(progress) {
            return (<ProgressBox height="255px" />)
        } else {
            return this.props.displayItemHandler()
        }
    };
    
    render() {
        return (
            <div className="CardList">
                {this.displayContent(this.props.progress)}
            </div>
        );
    }
}

ListOfFeedJobCards.propTypes = {
    displayItemHandler: PropTypes.func.isRequired,
    progress: PropTypes.bool,
};

ListOfFeedJobCards.defaultProps = {
    progress: false,
    height: "100%",
    overflowY: "hidden"
};
