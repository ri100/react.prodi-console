export default function(state={ data: [], error: null, progress: true }, action) {
    switch (action.type) {
        case "SEARCH_LOADING":
            return {
                ...state,
                data:[],
                error: null,
                progress: true
            };
        case "SEARCH_LOADED":
            return { 
                ...state,
                data: action.data,
                error: null,
                progress: false
            };
        case "SEARCH_FAILED":
            return {
                ...state,
                data: [],
                error: action.error,
                progress: true
            };
    }

    return state;
}