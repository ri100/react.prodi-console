import React from 'react';
import "./VerticalMenu.css";
import { Menu, Home, CloudDownload, Search, Style, PersonalVideo } from '@material-ui/icons';
import VerticalMenuLink from './VerticalMenuLink.jsx';

export default function VerticalMenu(props) {
    return <section id="appMenu" style={{ width: props.width + 'px' }}>
        <VerticalMenuLink label="Menu" full={props.full}>
            <Menu style={{ fontSize: '30px' }} onClick={props.menuToggle} />
        </VerticalMenuLink>
        <VerticalMenuLink label="Dashboard" link="/" full={props.full}>
            <Home style={{ fontSize: '30px' }} />
        </VerticalMenuLink>
        <VerticalMenuLink label="Feed's jobs" link="/jobList" full={props.full}>
            <CloudDownload style={{ fontSize: '30px' }} />
        </VerticalMenuLink>
        <VerticalMenuLink label="Providers" full={props.full}>
            <Search style={{ fontSize: '30px' }} onClick={props.openSearchDialog} />
        </VerticalMenuLink>
        <VerticalMenuLink label="Search data" link="/offers" full={props.full}>
            <Style style={{ fontSize: '30px' }} />
        </VerticalMenuLink>
        <VerticalMenuLink label="Console" link="/console" full={props.full}>
            <PersonalVideo style={{ fontSize: '27px', marginTop: '2px' }} />
        </VerticalMenuLink>
    </section>
}