import React from 'react';
import UpdateButton from './Misc/UpdateButton.jsx';
import { runAllFeedsFromProvider } from '../repo/providerDataApi.js'
import Snackbar from '@material-ui/core/Snackbar';
import format from '../logic/numberFormat';
import PropTypes from 'prop-types';

export default class ProviderTopInfo extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            openSnackBar: false,
            snackMessage: '',
            selectedTab: 1,
            updateButtonBgColor: 'transparent'
        }
    }
    
    updateClicked = () => {
        runAllFeedsFromProvider(
            this.props.id,
            (response)=> {
                this.setState({
                    snackMessage: response.data + ' feeds accepted for retrieval.',
                    openSnackBar: true,
                    updateButtonBgColor: 'green'
                });
            }, // on success
            (error)=> {
                this.setState({
                    snackMessage: error.message,
                    openSnackBar: true,
                    updateButtonBgColor: 'red'
                });
            } // on error
        );
    };

    render() {
        return (
            <div style={{display: 'table', width: '100%', height: '150px'}}>
                <div style={{display: 'table-cell'}}>
                    <div style={{display: 'table', width: '100%'}}>
                        <div style={{display: 'table-cell', width: '30%', padding: '10px 30px', whiteSpace: 'nowrap'}}>
                            <div style={{fontSize: '320%', fontWeight: 400, marginTop: '8px', marginRight: '60px'}}>
                                {this.props.name} <span style={{fontWeight: 300, fontSize: '70%'}}>data provider details</span>
                            </div>
                            <div style={{fontSize: '110%', fontWeight: 300, color: '#ccc'}}>
                                {this.props.url} <UpdateButton
                                    onClick={this.updateClicked}
                                    backgroundColor={this.state.updateButtonBgColor}>update</UpdateButton>
                            </div>
                        </div>
                        <div style={{display: 'table-cell'}}>
                            <table>
                                <tbody>
                                <tr>
                                    <td>
                                        <span style={{fontSize: '80%', fontWeight: 300, textTransform:'uppercase'}}>Address:</span><br />
                                        {(this.props.street) ? this.props.street : 'n/a'} {(this.props.town) ? this.props.town: 'n/a'}
                                    </td>
                                    <td style={{padding:"20px"}}><span
                                        style={{fontSize: '80%', fontWeight: 300, textTransform:'uppercase'}}>Hash:</span><br />
                                        {this.props.id}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span style={{fontSize: '80%', fontWeight: 300, textTransform:'uppercase'}}>Phone:</span><br />
                                        {(this.props.phone) ? this.props.phone : 'n/a'}
                                    </td>
                                    <td style={{padding:"20px"}}><span
                                        style={{fontSize: '80%', fontWeight: 300, textTransform:'uppercase'}}>Email:</span><br />
                                        {(this.props.email) ? this.props.email : 'n/a'}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div style={{padding: '10px 30px 0 30px'}}>
                        {this.props.children}
                    </div>
                </div>
                <div style={{display: 'table-cell', width: '400px', verticalAlign: 'middle'}}>
                    <div style={{textAlign:'right', paddingRight: '30px'}}>
                        <div style={{textTransform:'uppercase', fontWeight: 300}}>
                            Data
                        </div>
                        <div style={{fontSize: '400%', fontWeight: 300}}>{format(this.props.records,3,3,' ',',')}K</div>
                        <div style={{fontSize: '80%', fontWeight: 300}}>{this.props.date}</div>
                    </div>

                </div>
                <Snackbar
                    open={this.state.openSnackBar}
                    message={this.state.snackMessage}
                    autoHideDuration={4000}/>
            </div>
        );
    }
}

ProviderTopInfo.propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    street: PropTypes.string,
    town: PropTypes.string,
    phone: PropTypes.string,
    email: PropTypes.string,
    records: PropTypes.number.isRequired,
    date: PropTypes.string.isRequired,
};