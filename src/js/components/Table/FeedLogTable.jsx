import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import {Tag} from '../Misc/Tag.jsx';
import PropTypes from 'prop-types';
import ProgressLogConsole from "../ProgressLogConsole.jsx";
import ConsoleIcon from "../Dialog/ConsoleIcon.jsx";
import {ParseStatusBox, DownloadStatusBox, ProcessStatusBox} from "../Misc/StatusBox.jsx";

export default class FeedLogTable extends React.Component {

    renderRows = () => {
        return this.props.data
            .map((row, index) => (
                    <TableRow key={index}>
                        <TableCell
                            style={{fontSize: "100%", padding: "0 5px", textAlign: 'center'}}>{row.id}</TableCell>
                        <TableCell style={{fontSize: "100%", textAlign: 'center', padding: "5px", color: 'white'}}>
                            <Tag title="Source"
                                 backgroundColor={(row.localCopy === true) ? '#f57c00' : 'rgb(21, 101, 192)'}>{(row.localCopy === true) ? 'file' : 'url'}</Tag>
                        </TableCell>
                        <TableCell style={{fontSize: "100%", padding: "0 5px", textAlign: 'center'}}>
                            <DownloadStatusBox title={row.download_status_message} status={row.download_status}/>
                        </TableCell>
                        <TableCell style={{fontSize: "100%", padding: "0 5px", textAlign: 'center'}}>
                            <ParseStatusBox title={row.parse_status_message} status={row.parse_status}/>
                        </TableCell>
                        <TableCell style={{fontSize: "100%", padding: "0 5px", textAlign: 'center'}}>
                            <ProcessStatusBox title={row.process_status_message} status={row.process_status}/>
                        </TableCell>
                        <TableCell
                            style={{fontSize: "100%", padding: "0 5px", textAlign: 'center'}}>{row.name}</TableCell>
                        <TableCell
                            style={{fontSize: "100%", padding: "0 5px", textAlign: 'right'}}>{row.matched}</TableCell>
                        <TableCell
                            style={{fontSize: "100%", padding: "0 5px", textAlign: 'right'}}>{row.inserted}</TableCell>
                        <TableCell
                            style={{fontSize: "100%", padding: "0 5px", textAlign: 'right'}}>{row.updated}</TableCell>
                        <TableCell
                            style={{fontSize: "100%", padding: "0 5px", textAlign: 'right'}}>{row.deleted}</TableCell>
                        <TableCell
                            style={{fontSize: "100%", padding: "0 5px", textAlign: 'right'}}>{row.failed}</TableCell>
                        <TableCell style={{
                            fontSize: "100%",
                            padding: "0 5px",
                            textAlign: 'right'
                        }}>{row.elapsedTime} sec.</TableCell>
                        <TableCell style={{fontSize: "100%", padding: "0 5px"}}>
                            <ConsoleIcon
                                label="Console"
                                title="Logs"
                                OKLabel="Close">
                                <ProgressLogConsole jobId={row.id}/>
                            </ConsoleIcon>
                        </TableCell>
                    </TableRow>
                )
            );
    };

    render() {
        return (
            <Table height="100%">
                <TableHead>
                    <TableRow>
                        <TableCell style={{fontSize: "100%", padding: "0 5px", textAlign: 'center'}}>Job
                            Id</TableCell>
                        <TableCell style={{fontSize: "100%", padding: "0 5px", textAlign: 'center'}}
                                   tooltip="Data source">Source</TableCell>
                        <TableCell style={{fontSize: "100%", padding: "0 5px", textAlign: 'center'}}
                                   tooltip="Data source">Download</TableCell>
                        <TableCell style={{fontSize: "100%", padding: "0 5px", textAlign: 'center'}}
                                   tooltip="Data source">Parse</TableCell>
                        <TableCell style={{fontSize: "100%", padding: "0 5px", textAlign: 'center'}}
                                   tooltip="Data source">Process</TableCell>
                        <TableCell
                            style={{fontSize: "100%", padding: "0 5px", textAlign: 'center'}}>Date</TableCell>
                        <TableCell style={{fontSize: "100%", padding: "0 5px", textAlign: 'center'}}
                                   tooltip="Acquired items">Matched</TableCell>
                        <TableCell style={{
                            fontSize: "100%",
                            padding: "0 5px",
                            textAlign: 'center'
                        }}>Inserted</TableCell>
                        <TableCell style={{
                            fontSize: "100%",
                            padding: "0 5px",
                            textAlign: 'center'
                        }}>Updated</TableCell>
                        <TableCell style={{
                            fontSize: "100%",
                            padding: "0 5px",
                            textAlign: 'center'
                        }}>Deleted</TableCell>
                        <TableCell
                            style={{fontSize: "100%", padding: "0 5px", textAlign: 'center'}}>Failed</TableCell>
                        <TableCell style={{fontSize: "100%", padding: "0 5px", textAlign: 'center'}}
                                   tooltip="Time consumed during the the job">Speed</TableCell>
                        <TableCell/>
                    </TableRow>
                </TableHead>
                <TableBody>

                    {this.renderRows()}

                </TableBody>
            </Table>
        );
    }
}
FeedLogTable.propTypes = {
    data: PropTypes.array.isRequired,
};