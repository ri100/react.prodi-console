export default function(state={ value: 1 }, action) {
    switch (action.type) {
        case "INC":
            return { value: state.value + action.value };
        case "DEC":
            return { value: state.value - action.value };
    }

    return state;
}