import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import PropTypes from 'prop-types';

const NullNotAvailable = props => {
    const result = (props.value === null || typeof props.value == 'undefined') ? 'n/a' : props.value;
    return <div style={{position: "relative"}}>{result}</div>;
};

NullNotAvailable.propTypes = {
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ])
};
NullNotAvailable.defaultProps = {
    value: null
};

export default NullNotAvailable;
