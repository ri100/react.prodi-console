export default function(state={ data: [], error: null, progress: true }, action) {
    switch (action.type) {
        case "NOT_MATCHED_FEEDS_LOADING":
            return {
                ...state,
                data:[],
                error: null,
                progress: true
            };
        case "NOT_MATCHED_FEEDS_LOADED":
            return {
                ...state,
                data: action.data,
                error: null,
                progress: false
            };
        case "NOT_MATCHED_FEEDS_FAILED":
            return {
                ...state,
                data: [],
                error: action.error,
                progress: true
            };
    }

    return state;
}