import React from 'react';
import format from '../logic/numberFormat';
import PropTypes from 'prop-types';

export default class ProviderData extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (<div className="providerData">
            <div className="header">
                <h1 className="name">{this.props.name}</h1>
                <h2 className="url">{this.props.url}</h2>
            </div>
        </div>);
    }
}

ProviderData.propTypes = {
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    street: PropTypes.string,
    town: PropTypes.string,
    email: PropTypes.string,
    phone: PropTypes.string,
};
ProviderData.defaultProps = {
    street: 'n/a',
    town: 'n/a',
    email: 'n/a',
    phone: 'n/a',
};
