import React from 'react';
import DialogMsgSingleButton from './DialogMsgSingleButton.jsx';
import PropTypes from 'prop-types';
import PersonalVideoIcon from '@material-ui/icons/PersonalVideo';

export default class ConsoleIcon extends React.Component {

    constructor(props) {
        super(props);
    }

    handleClick = (e) => {
        e.preventDefault();
        e.stopPropagation();
        this.dialog.setState({
            openDialog: true
        });
    };

    render() {
        return (
            <span style={{display: "inline"}}>
                 <PersonalVideoIcon style={{height: '23px', cursor: "pointer", marginLeft: "5px"}}
                       onClickCapture={this.handleClick}/>
                 <DialogMsgSingleButton
                     title={this.props.title}
                     open={false}
                     OKLabel={this.props.OKLabel}
                     ref={(dialog) => {
                         this.dialog = dialog;
                     }}>
                     {this.props.children}
                 </DialogMsgSingleButton>
            </span>
        );
    }
}

ConsoleIcon.propTypes = {
    label: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    OKLabel: PropTypes.string,
};
ConsoleIcon.defaultProps = {
    OKLabel: 'OK',
};
