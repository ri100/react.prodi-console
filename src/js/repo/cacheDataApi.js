import { loadJson } from '../logic/axiosLoad';

export function getCacheAggregate(onSuccess, onError) {
    var url = 'api/CacheStats.aggregate';
    var params = {};
   loadJson(url, params, onSuccess, onError);
}