import React from 'react';
import PropTypes from 'prop-types';

export const ProcessStatusBox = props => {

    let renderProcessStatusColor = () => {
        if(!props.status) {
            return 'rgb(50, 50, 50)';
        } else if(props.status === 'parsing' || props.status === 'downloading' || props.status === 'started' || props.status === 'stopped' || props.status === 'syncing') {
            return 'rgb(100, 100, 100)';
        } else if(props.status === 'failed') {
            return 'rgb(255, 77, 0)';
        } else if(props.status !== 'finished') {
            return 'rgb(255, 0, 0)';
        } else {
            return 'rgb(46, 125, 50)';
        }
    };

    return <span title={props.title} className="statusBox" style={{backgroundColor: renderProcessStatusColor()}}>
        {props.status ? props.status : "n/a"}
    </span>
}
ProcessStatusBox.propTypes = {
    status: PropTypes.string,
    title: PropTypes.string
};

export const DownloadStatusBox = props => {

    let renderDownloadStatusColor = () => {
        if (!props.status) {
            return 'rgb(50, 50, 50)';
        } else if (props.status === 'stopped') {
            return 'rgb(100, 100, 100)';
        } else if (props.status === 'started' || props.status === 'pending') {
            return '#4e342e';
        } else if (props.status === 'Ok') {
            return 'rgb(46, 125, 50)';
        } else if (props.status === 'failed') {
            return 'rgb(255, 77, 0)';
        } else if (props.status !== 'finished') {
            return 'rgb(255, 0, 0)';
        } else {
            return 'rgb(46, 125, 50)';
        }
    };
    return <span title={props.title} className="statusBox" style={{backgroundColor: renderDownloadStatusColor()}}>
        {props.status ? props.status : "n/a"}
    </span>
};
DownloadStatusBox.propTypes = {
    status: PropTypes.string,
    title: PropTypes.string
};

export const ParseStatusBox = props => {

    let renderParseStatusColor = () => {
        if (!props.status) {
            return 'rgb(50, 50, 50)';
        } else if (props.status === 'started' || props.status === 'pending') {
            return '#4e342e';
        } else if (props.status === 'stopped') {
            return 'rgb(100, 100, 100)';
        } else if (props.status !== 'finished') {
            return 'rgb(255, 0, 0)';
        } else {
            return 'rgb(46, 125, 50)';
        }
    };
    return <span title={props.title} className="statusBox" style={{backgroundColor: renderParseStatusColor()}}>
        {props.status ? props.status : "n/a"}
    </span>
};
ParseStatusBox.propTypes = {
    status: PropTypes.string,
    title: PropTypes.string
};