import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import Switch from '@material-ui/core/Switch';
import UpdateButton from './Misc/UpdateButton.jsx';
import { hashHistory } from 'react-router';
import format from '../logic/numberFormat';
import { runFeed, getDetails, toggleActiveFeed } from '../repo/feedDataApi.js'
import PropTypes from 'prop-types';

export default class FeedTopInfo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 1,
            updateButtonBgColor: 'transparent',
            records: format(props.records, 3, 3, ' ', ',') + 'K',
            status: props.status,
            openSnackBar: false,
            snackMessage: '',
        }
    }

    displayError = (error) => {
        this.setState({
            openSnackBar: true,
            snackMessage: error.message
        });
    };

    activeToggle = (toggled, feedId) => {
        toggleActiveFeed(
            feedId,
            toggled,
            () => {console.log("Feed " + feedId + " toggled")},
            this.displayError);
    };
    
    componentDidMount = () => {
        if(this.state.status === 'started' || this.state.status === 'downloading' || this.state.status === 'parsing') {
            this.interval = setInterval(this.updateRecords, 2000);
        }
    };

    updateRecords = () => {
        getDetails(
            this.props.feedId,
            (response) => {
                if(response.data.download.status.code === 'finished') {
                    this.setState({
                        status: response.data.process.status.code,
                        records: format(response.data.parse.matched/1000, 3, 3, ' ', ',') + 'K',
                        openSnackBar: false
                    });
                } else if(response.data.download.status.code === 'started') {
                    if(response.data.download.progress.type === 'percentage') {
                        this.setState({
                            status: response.data.process.status.code,
                            records: response.data.download.progress.value + '%',
                            openSnackBar: false
                        });
                    } else {
                        this.setState({
                            status: response.data.process.status.code,
                            records: format(response.data.download.progress.value/1024, 1, 3, ' ', ',') + 'mb',
                            openSnackBar: false
                        });
                    }
                } else {
                    this.setState({
                        status: response.data.process.status.code,
                        openSnackBar: false
                    });
                }

                var status = response.data.process.status.code;
                if(status === 'finished' || status === 'internal-error' || status === 'failed') {
                    clearInterval(this.interval);
                    if(this.props.onFeedFinished) {
                        this.props.onFeedFinished();
                    }
                    this.setState({
                        status: response.data.process.status.code,
                        records: format(response.data.parse.matched/1000, 3, 3, ' ', ',') + 'K',
                        openSnackBar: false
                    });
                }

            },
            () =>  {
                clearInterval(this.interval);
            }
        );
    };

    componentWillUnmount = () => {
        clearInterval(this.interval);
    };

    feedUpdate = () => {
        runFeed(this.props.feedId, 
            () =>  {this.setState({
                updateButtonBgColor:'green',
                snackMessage: 'Feed job accepted',
                openSnackBar: true,
                });
                clearInterval(this.interval);
                this.interval = setInterval(this.updateRecords, 2000);
            },
            (error) =>  this.setState({
                snackMessage: error.message,
                openSnackBar: true,
                updateButtonBgColor:'red'
            }));
    };
    
    goToProvider = () => {
        hashHistory.push('/providerDetails/'+this.props.providerId)
    };
    
    render() {
        return (
            <div style={{display: 'table', width: '100%', height: '150px'}}>
                <div style={{display: 'table-cell'}}>
                    <div style={{display: 'table', width: '100%'}}>
                        <div style={{display: 'table-cell', padding: '10px 30px', whiteSpace: 'nowrap', minWidth: '450px'}}>
                            <div style={{fontSize: '320%', fontWeight: 400, marginTop: '8px', marginRight: '60px'}}>
                                <div style={{maxWidth:'250px', overflow: 'hidden', textOverflow: 'ellipsis', float:'left', marginRight: '10px'}}>{this.props.name}</div>
                                <span style={{fontWeight: 300, fontSize: '70%'}}>{(this.props.download_status=='failed') ? <span style={{color:'#F06292'}} title={this.props.download_status_message}>failed</span> : 'feed details'}</span>
                            </div>
                            <div style={{fontSize: '110%', fontWeight: 300, color: '#ccc', verticalAlign: 'middle'}}>
                                <div style={{width:'250px', overflow: 'hidden', textOverflow: 'ellipsis', float:'left'}}>{this.props.url}</div>
                                <UpdateButton onClick={this.goToProvider}>Go to provider</UpdateButton> 
                                <UpdateButton onClick={this.feedUpdate} backgroundColor={this.state.updateButtonBgColor}>update</UpdateButton>
                            </div>
                        </div>
                        <div style={{display: 'table-cell', verticalAlign: 'bottom', width:'140px'}}>
                            <div style={{fontSize: '80%', fontWeight: 300, textTransform:'uppercase'}}>Active:</div>
                            <Switch defaultChecked={this.props.active} onChange={(e, toggled) => { this.activeToggle(toggled, this.props.feedId) }}/>
                            <div style={{fontSize: '80%', fontWeight: 300, textTransform:'uppercase'}}>Schema:</div>
                            <div style={{width:'140px', overflow: 'hidden', textOverflow: 'ellipsis'}}>{this.props.schema}</div>
                        </div>
                        <div style={{display: 'table-cell', verticalAlign: 'bottom'}}>
                            <div style={{marginTop:'10px',fontSize: '80%', fontWeight: 300, textTransform:'uppercase'}}>Provider hash:</div>
                            {this.props.hash}
                            <div style={{marginTop:'10px',fontSize: '80%', fontWeight: 300, textTransform:'uppercase'}}>Feed Id:</div>
                            {this.props.feedId}
                        </div>
                    </div>
                    <div style={{padding: '10px 30px 0 30px'}}>
                        {this.props.children}
                    </div>
                </div>
                <div style={{display: 'table-cell', width: '400px', verticalAlign: 'middle'}}>
                    <div style={{textAlign:'right', paddingRight: '30px'}}>
                        <div style={{textTransform:'uppercase', fontWeight: 300}}>
                            Records
                        </div>
                        <div style={{fontWeight: 300}}>{this.state.status} <span style={{fontSize: '400%'}}>{ this.state.records }</span></div>
                        <div style={{fontSize: '80%', fontWeight: 300}}>{this.props.date}</div>
                    </div>

                </div>
                <Snackbar
                    open={this.state.openSnackBar}
                    message={this.state.snackMessage}
                    autoHideDuration={4000}
                />
            </div>
        );
    }
}

FeedTopInfo.propTypes = {
    feedId: PropTypes.string.isRequired,
    providerId: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    schema: PropTypes.string.isRequired,
    hash: PropTypes.string.isRequired,
    records: PropTypes.number.isRequired,
    date: PropTypes.string.isRequired,
    active: PropTypes.bool.isRequired,
    download_status: PropTypes.string.isRequired,
    download_status_message: PropTypes.string,
    onFeedFinished: PropTypes.func
};

FeedTopInfo.defaultProps = {
    download_status_message: null
};