import { loadJson } from '../logic/axiosLoad';

export function startAllJobs(onSuccess, onError) {
    var url = 'api/Application.startAllJobs';
    var params = {};
   loadJson(url, params, onSuccess, onError);
}

export function startInActiveJobs(onSuccess, onError) {
    var url = 'api/Application.startInActiveJobs';
    var params = {};
    loadJson(url, params, onSuccess, onError);
}

export function startFailedJobs(onSuccess, onError) {
    var url = 'api/Application.startFailedJobsIn24h';
    var params = {};
    loadJson(url, params, onSuccess, onError);
}

export function startJobsWithParseErrors(onSuccess, onError) {
    var url = 'api/Application.startJobsWithParseErrorsIn24h';
    var params = {};
    loadJson(url, params, onSuccess, onError);
}

export function startJobsWithNotMatchedFeeds(onSuccess, onError) {
    var url = 'api/Application.startJobsWithNotMatchedFeeds';
    var params = {};
    loadJson(url, params, onSuccess, onError);
}

