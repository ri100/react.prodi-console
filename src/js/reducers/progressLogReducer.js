export default function(state={ data: [], error: null, progress: true }, action) {
    switch (action.type) {
        case "LOG_LOADING":
            return {
                ...state,
                data: [],
                error: null,
                progress: true
            };
        case "LOG_LOADED":
            return {
                ...state,
                data: action.data,
                error: null,
                progress: false
            };
        case "LOG_FAILED":
            return {
                ...state,
                data: [],
                error: action.error,
                progress: true
            };
    }

    return state;
}