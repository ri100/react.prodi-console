import React from 'react';
import {ProgressBox} from '../Misc/ProgressBox.jsx'
import {
    ComposedChart,
    ResponsiveContainer,
    XAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    Bar
} from 'recharts';
import PropTypes from 'prop-types';

const CacheFailureBarChart = props => {
    const renderCacheFailuresChart = (props) => {

        if (props.dataSource.progress) {
            var height = props.height+'px';
            return (<ProgressBox height={height}/>);
        }

        return (<ResponsiveContainer minHeight={props.height}>
            <ComposedChart
                style={{fontFamily: 'Lato, sans-serif', fontWeight: '400', fontSize: "12px"}}
                data={props.dataSource.data}>

                <XAxis dataKey="date"/>
                <Tooltip label="Legend"/>
                <Bar
                    name="Cache failures"
                    dataKey="count"
                    fill='#FFAB91'
                />
                <Legend />
            </ComposedChart>
        </ResponsiveContainer>);
    };
    
    return renderCacheFailuresChart(props);
};

CacheFailureBarChart.propTypes = {
    height: PropTypes.number,
    dataSource: PropTypes.object.isRequired
};
CacheFailureBarChart.defaultProps = {
    height: '100%'
};

export default CacheFailureBarChart;
