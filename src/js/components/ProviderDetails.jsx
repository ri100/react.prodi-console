import React from 'react';
import Paper from '@material-ui/core/Paper';
import FeedTable from './Table/FeedTable.jsx';
import DivLink from './Misc/DivLink.jsx';
import {connect} from "react-redux";
import { getFeedListForProvider } from "../actions/feedActions";
import { getProviderJobsChartData } from "../actions/feedLogActions";
import { getProviderDetails } from "../actions/providersActions";
import ProviderTopInfo from './ProviderTopInfo.jsx';
import { ProgressBox } from './Misc/ProgressBox.jsx'
import {
    ComposedChart,
    Bar,
    ResponsiveContainer,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    Line
} from 'recharts';

@connect((store) => {
    return {
        sourceList: store.sourceList,
        providerJobsChartData: store.providerJobsChartData,
        providerDetails: store.providerDetails
    }
})
export default class ProviderDetails extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            chartType: 1
        }
    }

    componentDidMount = () => {
        this.props.dispatch(getFeedListForProvider(this.props.params.id));
        this.props.dispatch(getProviderJobsChartData(this.props.params.id));
        this.props.dispatch(getProviderDetails(this.props.params.id));
    };

    showDetails = (sourceId) => {
        console.log(sourceId);
    };

    handleClick= () => {};

    renderChart = () => {
        if (this.state.chartType == 1) {
            return this.renderDeltaChart();
        } else if (this.state.chartType == 2) {
            return this.renderDataChart();
        } else if (this.state.chartType == 3) {
            return this.renderCacheChart();
        } else if (this.state.chartType == 4) {
           
        } else if (this.state.chartType == 5) {
           
        }
        return this.renderDeltaChart();
    };

    renderDataChart = () => {

        if(this.props.providerJobsChartData.progress) {
            return (<ProgressBox height="240px" />);
        }

        return (<ResponsiveContainer minHeight={240}>
            <ComposedChart
                style={{backgroundColor: '#e5e5e5', fontFamily: 'Lato, sans-serif', fontWeight: '400', fontSize: "12px"}}
                data={this.props.providerJobsChartData.data} margin={{ top: 30, right: 20, left: 0, bottom: 0 }}>
                <XAxis dataKey="name"/>
                <YAxis/>
                <CartesianGrid strokeDasharray="3 3"/>
                <Tooltip label="Legend"/>
                <Legend />

                <Line name="Matched items" dataKey='matched' stroke="gray"
                      dot={{r: 6}} strokeWidth={2} onClick={this.handleClick}/>

            </ComposedChart>
        </ResponsiveContainer>);
    };
    
    renderCacheChart = () => {

        if(this.props.providerJobsChartData.progress) {
            return (<ProgressBox height="240px" />);
        }

        return (<ResponsiveContainer minHeight={240}>
            <ComposedChart
                style={{backgroundColor: '#e5e5e5', fontFamily: 'Lato, sans-serif', fontWeight: '400', fontSize: "12px"}}
                data={this.props.providerJobsChartData.data} margin={{ top: 30, right: 20, left: 0, bottom: 0 }}>
                <XAxis dataKey="name"/>
                <YAxis/>
                <CartesianGrid strokeDasharray="3 3"/>
                <Tooltip label="Legend"/>
                <Legend />

                <Line name="Not synchronized data between database and cache" dataKey='unSyncDatabaseData' stroke="red"
                      dot={{r: 6}} strokeWidth={2} onClick={this.handleClick}/>

                <Line name="Not synchronized data inside cache" dataKey='unSyncCacheData' stroke="#424242"
                      dot={{r: 6}} strokeWidth={2} onClick={this.handleClick}/>

            </ComposedChart>
        </ResponsiveContainer>);
    };

    renderDeltaChart = () => {

        if(this.props.providerJobsChartData.progress) {
            return (<ProgressBox height="240px" />);
        }

        return (<ResponsiveContainer minHeight={240}>
            <ComposedChart
                style={{backgroundColor: '#e5e5e5', fontFamily: 'Lato, sans-serif', fontWeight: '400', fontSize: "12px"}}
                data={this.props.providerJobsChartData.data} margin={{ top: 30, right: 20, left: 0, bottom: 0 }}>
                <XAxis dataKey="name"/>
                <YAxis/>
                <CartesianGrid strokeDasharray="3 3"/>
                <Tooltip label="Legend"/>
                <Legend />

                <Bar stackId="all" name="Deleted items" dataKey="deleted"
                     fill="#F48FB1" onClick={this.handleClick}/>
                <Bar stackId="all" name="Matched items" dataKey="matched"
                     fill="#2196F3" onClick={this.handleClick}/>
                <Line name="Failed items" dataKey="failed"
                     stroke="#666666" dot={{r: 6}} strokeWidth={2}  onClick={this.handleClick}/>

            </ComposedChart>
        </ResponsiveContainer>);
    };

    renderTable = () => {
        if(this.props.sourceList.progress) {
            return (<ProgressBox height="100px" />);
        }

        return (<FeedTable data={this.props.sourceList.data} showDetailsEventFunc={this.showDetails} />);
    };
    
    renderTopInfo = () => {
       
        if(!this.props.providerDetails.progress) {
            if(this.props.providerDetails.data) {
                return <ProviderTopInfo
                    id={this.props.providerDetails.data.provider.hash}
                    name={this.props.providerDetails.data.provider.name}
                    url={this.props.providerDetails.data.provider.url}
                    street={this.props.providerDetails.data.provider.address.street}
                    town={this.props.providerDetails.data.provider.address.town}
                    phone={this.props.providerDetails.data.provider.address.phone}
                    email={this.props.providerDetails.data.provider.address.mail}
                    records={this.props.providerDetails.data.provider.report.numberOfProcessedData/1000}
                    date={this.props.providerDetails.data.provider.report.lastActionDate}
                >
                    <DivLink
                        defaultSelected={this.state.chartType == 1}
                        onClick={() => { this.setState({chartType: 1}) }}>Delta</DivLink>

                    <DivLink
                        defaultSelected={this.state.chartType == 2}
                        onClick={() => { this.setState({chartType: 2}) }}>Data</DivLink>

                    <DivLink
                        defaultSelected={this.state.chartType == 3}
                        onClick={() => { this.setState({chartType: 3}) }}>Cache</DivLink>

                </ProviderTopInfo>
            }
        }
    };
    
    render() {
        return (
            <div>

                <Paper elevation={2} style={{
                    width: '100%',
                    backgroundColor: '#BF360C',
                    color: 'white',
                    position: 'relative',
                    zIndex: 10
                }}>
                    {this.renderTopInfo()}
                </Paper>
                <Paper className="downloadDetails">
                    { this.renderChart() }
                    <div style={{padding: '10px'}}>
                        { this.renderTable() }
                    </div>
                </Paper>
            </div>
        );
    };
}