import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import PropTypes from 'prop-types';

export const ProgressBox = props => {

    const progressContainer = {
        width: props.width,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: props.height
    };

    return (
        <div style={progressContainer}>
            <CircularProgress />
        </div>
    );
};

ProgressBox.propTypes = {
    width: PropTypes.string,
    height: PropTypes.string.isRequired
};
ProgressBox.defaultProps = {
    width: '100%',
};
