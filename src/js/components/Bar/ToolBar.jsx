import React from 'react';
import AutoComplete from '@material-ui/core/AutoComplete';
import Paper from '@material-ui/core/Paper';
import LogInDialog from '../Dialog/LogInDialog.jsx';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import getMuiTheme from '@material-ui/core/styles/getMuiTheme';
import {Link} from 'react-router';
import Button from '@material-ui/core/Button';
import {searchProvider} from "../../actions/providersActions";
import {connect} from "react-redux";
import {hashHistory} from 'react-router';

import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from '@material-ui/core/Toolbar';
import PropTypes from 'prop-types';

const toolbarTheme = getMuiTheme({
    toolbar: {
        backgroundColor: '#e0e0e0',
        color: 'black'
    },
    fontFamily: 'Lato, sans-serif'
});

@connect((store) => {
    return {};
})
export default class ToolBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            value: 1,
            dataSource: ['tet', 'test']
        };
    }

    handleOpen = () => {
        this.refs.login.setState({open: true});
    };

    search = (query, index) => {
        hashHistory.push('/providers');
        this.props.dispatch(searchProvider(query));
    };

    render() {
        return (
            <div>
                <MuiThemeProvider muiTheme={toolbarTheme}>
                    <Paper elevation={3}>
                        <Toolbar style={this.props.style}>
                            <ToolbarGroup firstChild={true}>
                                <Link to={"/downloads"}>
                                    <Button variant="contained"
                                            primary={false}
                                            style={{marginTop: "9px", marginRight: '9px', marginLeft: '9px'}}
                                    >Jobs</Button></Link>
                                <Link to={"/providers"}>
                                    <Button variant="contained"
                                            primary={false}
                                            style={{marginTop: "9px", marginRight: '9px'}}
                                    >Providers</Button></Link>
                            </ToolbarGroup>
                            <ToolbarGroup>
                                <AutoComplete
                                    placeholder="Search"
                                    dataSource={this.state.dataSource}
                                    onNewRequest={this.search}
                                />
                                <ToolbarSeparator/>
                                <Button variant="contained" primary={true} onClick={this.handleOpen}>Log-in</Button>
                            </ToolbarGroup>
                        </Toolbar>
                    </Paper>
                </MuiThemeProvider>
                <LogInDialog ref="login"/>
            </div>
        );
    }
}


ToolBar.propTypes = {
    style: PropTypes.object,
};
