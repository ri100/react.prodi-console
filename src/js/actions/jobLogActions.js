import { loadViaRedux } from '../logic/reduxAction.js';

export function logData(jobId) {
    let url = 'api/JobLog.getLog';
    let params = {
        jobId: jobId,
        limit: 250
    };
    return loadViaRedux('LOG', url, params);
}

export function logsData() {
    let url = 'api/JobLog.getLogs';
    let params = {
        limit: 150
    };
    return loadViaRedux('LOGS', url, params, false);
}

export function errorData(jobId) {
    let url = 'api/JobLog.getErrors';
    let params = {
        jobId: jobId,
        limit: 250
    };
    return loadViaRedux('ERROR_LOG', url, params);
}