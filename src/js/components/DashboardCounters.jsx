import React from 'react';
import UpdateButton from './Misc/UpdateButton.jsx';
import {
    startAllJobs, startInActiveJobs, startFailedJobs, startJobsWithParseErrors, startJobsWithNotMatchedFeeds
} from '../repo/dashboardDataApi';
import {getFeedsStats} from "../actions/dashboardActions";
import {hashHistory} from 'react-router';
import NullNotAvailable from './Misc/NullNotAvailable.jsx'
import {connect} from "react-redux";
import PropTypes from 'prop-types';

@connect((store) => {
    return {
        feedsStats: store.feedsStats
    }
})
export default class DashboardCounters extends React.Component {
    
    constructor(props) {
        super(props);
    }
    
    componentDidMount = () => {
        this.props.dispatch(getFeedsStats());
        this.interval = setInterval(() => {
            this.props.dispatch(getFeedsStats())
        }, 10000);
    };

    componentWillUnmount = () => {
        clearInterval(this.interval);
    };
    
    render() {

        const tableStyle = {
            display: 'table',
            width: '100%',
            color: 'white',
            backgroundColor: '#D84315',
            padding: '10px',
            boxSizing: 'border-box',
            height: '135px'
        };

        const tableCell = {
            display: 'table-cell',
            padding: '10px 20px 0 10px',
            borderLeft: 'solid 2px white'
        };

        const counterStyle = {
            fontSize: '300%',
            fontWeight: 300,
            height: '60px'

        };

        const miniCounterStyle1 = {
            fontSize: '150%',
            fontWeight: 300,
            height: '40px'

        };

        const miniCounterStyle2 = {
            fontSize: '150%',
            fontWeight: 300,
            height: '30px'

        };
        
        const titleStyle = {
            fontSize: '110%',
        };

        return (
            <div style={tableStyle}>
                <div style={tableCell}>
                    <div style={titleStyle}>Active feeds</div>
                    <div style={counterStyle}><NullNotAvailable value={this.props.feedsStats.data.activeFeeds}/></div>
                    {(this.props.feedsStats.data.activeFeeds > 0) ? <UpdateButton onClick={
                            () => {
                                startAllJobs(
                                    (response) => {
                                        this.props.onSuccess(response.data)
                                    },
                                    (error) => {
                                        this.props.onError(error)
                                    }
                                )
                            }
                        } marginLeft="0px">update all</UpdateButton> : '' }
                </div>
                <div style={tableCell}>
                    <div style={titleStyle}>Inactive feeds</div>
                    <div style={counterStyle}><NullNotAvailable value={this.props.feedsStats.data.inactiveFeeds}/></div>
                    {(this.props.feedsStats.data.inactiveFeeds > 0) ? <UpdateButton onClick={
                            () => {
                                startInActiveJobs(
                                    (response) => {this.props.onSuccess(response.data)},
                                    (error) => {this.props.onError(error)}
                                )
                            }
                        } marginLeft="0px">update</UpdateButton> : '' }
                </div>
                <div style={tableCell}>
                    <div style={titleStyle}>Working feeds</div>
                    <div style={counterStyle}><NullNotAvailable value={this.props.feedsStats.data.currentlyWorking}/></div>
                    <UpdateButton onClick={
                        () => {
                            hashHistory.push('/jobList');
                        }
                    } marginLeft="0px">show</UpdateButton>
                </div>
                <div style={tableCell}>
                    <div style={titleStyle}>Processed in 24h</div>
                    <div style={miniCounterStyle1}><NullNotAvailable value={this.props.feedsStats.data.processedIn24h}/></div>
                    <div style={titleStyle}>Processed today</div>
                    <div style={miniCounterStyle2}><NullNotAvailable value={this.props.feedsStats.data.processedToday}/></div>
                </div>
                <div style={tableCell}>
                    <div style={titleStyle}>Failed feed in 24h</div>
                    <div style={counterStyle}><NullNotAvailable value={this.props.feedsStats.data.withDownloadErrorsIn24h}/></div>
                    {(this.props.feedsStats.data.withDownloadErrorsIn24h > 0) ? <UpdateButton onClick={
                            () => {
                                startFailedJobs(
                                    (response) => {
                                        this.props.onSuccess(response.data)
                                    },
                                    (error) => {
                                        this.props.onError(error)
                                    }
                                )
                            }
                        } marginLeft="0px">update</UpdateButton> : '' }
                </div>
                <div style={tableCell}>
                    <div style={titleStyle}>Failed parsing in 24h</div>
                    <div style={counterStyle}><NullNotAvailable value={this.props.feedsStats.data.withParseErrorsIn24h}/></div>
                    {(this.props.feedsStats.data.withParseErrorsIn24h > 0) ? <UpdateButton onClick={
                            () => {
                                startJobsWithParseErrors(
                                    (response) => {
                                        this.props.onSuccess(response.data)
                                    },
                                    (error) => {
                                        this.props.onError(error)
                                    }
                                )
                            }
                        } marginLeft="0px">update</UpdateButton> : ''}
                </div>
                <div style={tableCell}>
                    <div style={titleStyle}>Not matched in 24h</div>
                    <div style={counterStyle}><NullNotAvailable value={this.props.feedsStats.data.notMatchedFeeds}/></div>
                    {(this.props.feedsStats.data.notMatchedFeeds > 0) ? <UpdateButton onClick={
                            () => {
                                startJobsWithNotMatchedFeeds(
                                    (response) => {
                                        this.props.onSuccess(response.data)
                                    },
                                    (error) => {
                                        this.props.onError(error)
                                    }
                                )
                            }
                        } marginLeft="0px">update</UpdateButton> : ''}
                </div>
            </div>)
    }
};

DashboardCounters.propTypes = {
    onSuccess: PropTypes.func.isRequired,
    onError: PropTypes.func.isRequired,
};
DashboardCounters.defaultProps = {
};