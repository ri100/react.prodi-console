export default function(state={ data: [], error: null, progress: true }, action) {
    switch (action.type) {
        case "FAILED_XML_LIST_LOADING":
            return {
                ...state,
                data:[],
                error: null,
                progress: true
            };
        case "FAILED_XML_LIST_LOADED":
            return {
                ...state,
                data: action.data,
                error: null,
                progress: false
            };
        case "FAILED_XML_LIST_FAILED":
            return {
                ...state,
                data: [],
                error: action.error,
                progress: true
            };
    }

    return state;
}