import { loadJson } from './axiosLoad';

export function loadViaRedux(name, url, params, loading = true) {
    return function (dispatch) {
        
        if(loading) {
            dispatch({type: name + '_LOADING'});
        }
        
        loadJson(
            url, 
            params,
            (response) =>  dispatch({type: name + '_LOADED', data: response.data}),
            (error) =>  dispatch({type: name + '_FAILED', error: error})
        );
    }
}