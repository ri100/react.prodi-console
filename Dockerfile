FROM httpd:2.2

# install prerequisites
RUN apt-get update
RUN apt-get install -y git curl

# install nodejs
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash
RUN apt-get update
RUN apt-get install -y nodejs
RUN nodejs --version
RUN npm install -g yarn

RUN mkdir /code
WORKDIR /code

# prepare public
RUN rm -f /usr/local/apache2/htdocs/index.html
COPY public /usr/local/apache2/htdocs
RUN rm -f /usr/local/apache2/htdocs/bundle.js

# copy src
COPY src ./src
RUN rm -f ./src/bundle.js
COPY package.json .
COPY yarn.lock .
RUN yarn install

# build
COPY webpack.config.js .
RUN mkdir public
RUN yarn run prod
RUN cp ./public/bundle.js /usr/local/apache2/htdocs
RUN ls /code

# public data
RUN chown -R www-data:www-data /usr/local/apache2/htdocs

