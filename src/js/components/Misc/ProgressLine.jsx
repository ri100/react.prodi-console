import React from 'react';

export const ProgressLine = props => {
    return <div className="progressLineContainer">
        <div className="progressLine" style={{width: props.children + "%"}}>{props.children}%</div>
    </div>;

};

