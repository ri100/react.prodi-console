import { loadViaRedux } from '../logic/reduxAction.js';

export function getProviderJobsChartData(providerId) {
    let url = 'api/FeedLog.getProviderJobs';
    let params = {
        providerId: providerId
    };
    return loadViaRedux('MERCHANT_JOBS', url, params);
}

export function getJobsChartData(downloadId) {
    let url = 'api/FeedLog.getJobChartDataByDownloadId';
    let params = {
        downloadId: downloadId
    };
    return loadViaRedux('JOB_CHART_DATA', url, params);
}

export function feedLogDetails(jobId) {
    let url = 'api/FeedLog.getDetails';
    let params = {
        jobId: jobId
    };
    return loadViaRedux('DOWNLOAD_LOG', url, params);
}